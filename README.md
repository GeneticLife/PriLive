PriLive: Privacy-preserving real-time filtering of Illumina reads
=======================================

Description
-----------

PriLive is a privacy-preserving real-time filtering tool that was designed to remove
human reads from Illumina HiSeq (or comparable) data right in the moment when they
are produced. This means, read filtering is finished as soon as the sequencer is
finished. Human sequence information is never completely available at a single point
in time when using PriLive. This enables a new level of genomic privacy for human-
related sequencing procedures. PriLive is suitable for genomic and metagenomic data
sets.   
   
Features:
   
 * ! Run in parallel to the sequencing machine !   
 * ! Remove sequence information from the original base call files !   
 * Support of a foreground reference genome for genomic data   
 * Suitable for metagenomic data by using a null reference genome   
 * High filtering sensitivity by a local alignment approach   
 * High specificity by setting the -e parameter to a maximal edit distance of interest for the foreground alignment


Website
-------

The PriLive project website is https://gitlab.com/lokat/PriLive   
   
There you can find the latest version of PriLive, source code, documentation and
examples.


Installation
------------

Please compile PriLive from source.
   
Make sure that the following dependencies are installed:

 * cmake (>= 2.8)
 * boost (system, filesystem, program\_options)
 * zlib
 * lz4
 * openssl (crypto)

If using a local version of lz4 then adjust path in CMakeLists.txt line 61.

---

You also need to download a specific seqan build. Navigate to a folder where
you want to store the seqan library and run:

    git clone https://github.com/seqan/seqan.git
    cd seqan && git checkout 9119aa6

---

Check out the PriLive source code from the project website:

	git clone https://gitlab.com/lokat/PriLive
	
Adjust the paths of the seqan module in the file CMakeLists.txt in the source 
folder (line 31 and 32). Then, compile PriLive with:

    cd [source-dir]
    mkdir build && cd build
    cmake ..
    make

---

To compile PriLive with a different k-mer size than k=15 make the following
adjustment (here: k=10):

    cmake -DPriLive\_K 10 ..
    make


Usage
-----

PriLive has two components:

 * ``prilive-build``  builds the k-mer index of the reference genome
 * ``prilive``         the read filtering software itself

---

#### Using prilive-build:

Building a k-mer index from FASTA file input.fa to output file input.fa.kix:

    prilive-build input.fa

Building an index from a large reference genome. Here it makes sense to use
trimming, i.e. removing k-mers from the index that occurr more than 1000 times (for
example) in the index. The index is written into the file trimmed.kix

    prilive-build -t 1000 -o trimmed.kix input.fa

---

#### Using prilive:

The general call of prilive is

	prilive -f $BGREF $BCLDIR $FGREF 216 $OUTDIR
	
where   
	$BGREF 	is the path to the background reference (.kix)   
	$BCLDIR is the path to the Illumina BaseCall directory   
	$FGREF 	is the path to the foreground reference (.kix)   
	$OUTDIR is the path to the output directory of PriLive   

Please type

	prilive --help
	// or
	man ./prilive.man
	
to see further optional parameters.

To perform a foreground alignment without applying read filtering functionality,
call prilive without declaring a background reference genome with the -f parameter.

Also see the following sections and the example files for details about the most
important functionalities.

---

#### Use with genomic data

For use with genomic data, unless there are special reasons for your data, we
recommend to only change the -e parameter.   
This parameter will affect

 * the sensitivity/specificity-ratio of the read filtering and
 * the results of the foreground alignment

The general meaning of the -e parameter is the permitted number of errors (edit
distance) for the foreground alignment. A higher -e parameter therefore means that
there must be more errors to identify a read as 'not foreground-related'. Thus, more
errors are required to remove the sequence information from the data.   
The --ign-start and --ign-discard parameters can be set to handle the specificity of
the read filtering independently to the permitted number of errors in the foreground
alignment.

---

#### Use with metagenomic data

For use with metagenomic data, it is recommended to not use a valid foreground
reference genome. Instead, a null-reference is used which contains no meaningful
sequence information. This null-reference can be created by building an index from a
FASTA file that only contains a sequence of the ambiguous base N.   
When running PriLive with such an index, we recommend to set -e to 0 and to increase
the necessary background alignment score to discard a read manually. The exact value
of this parameter can vary for different types of data, but because of the design of
the background alignment algorithm it often makes sense to set it slightly below
half of the read length (e.g., --ign-score 45 for reads of length 100).

---

#### Paired-end reads & Demultiplexing:

By default, PriLive interprets the complete number of cycles as a single read.   
Thus, by typing

	prilive -f $BG_REF $BCL_DIR $FG_REF 216 $OUT_DIR
	
all 216 cycles are tried to be aligned as a single sequence of length 216.   
If you want to handle paired-end sequencing, use the -r parameter to declare the
single fragments:

	prilive -r 100R 8B 8B 100R [...]
	        |-----------------|

To define the barcode sequence(s) of your sample, use the -b parameter.   
Thereby, the fragments of dual- or multiple-barcodes are concatenated with a '-':

	prilive -r 100R 8B 8B 100R -b AATCTAGG-GCGTAGGC ATCGGATC-TAGCGATTC [...]
	                           |--------------------------------------|

To allow errors in your barcode sequences, use the -E parameter:

	prilive -r 100R 8B 8B 100R -b AATCTAGG-GCGTAGGC ATCGGATC-TAGCGATTC -E 2 1 [...]
	                                                                   |-----|
	
This final command would interpret the complete 216 cycles as the sequence structure
100-8-8-100. The two considered dual-barcode sequences are declared by the -b
parameter. Thereby, the first part of each barcode can have up to 2 errors, the
second part at most 1. Please note that the barcodes are considered in both
foreground and background alignment. Thus, all reads that do not have the correct
barcode sequences will neither be mapped to the foreground reference genome, nor be
removed from the data by the read filtering functionality.

---

#### Threading

We recommend to adjust the numbers of threads used by PriLive with -n. If possible,
make use of as many threads as (#lanes * #tiles)!

Please consult the project website for more details on the parameters!

---

#### Disclaimer acceptance

PriLive is designed to remove data irrecoverably.
The developers of PriLive do not guarantee for any loss of data and any other
damage that is directly or indirectly caused by the use of PriLive.
Therefore, it is necessary to accept the disclaimer of PriLive every
time when using the software.
This can be done
 * manually at every start of the program
 * or by adding the optional parameter --accept-disclaimer for use in automated workflows

---

#### Relation to HiLive

HiLive (Linder et al., 2017) is a real-time read mapping software which was developed
in the Bioinformatics Unit (MF1) at Robert Koch Institute. PriLive is
based on the code of HiLive and therefore provides full HiLive functionality. The
algorithm of PriLive was developed independently from the HiLive foreground
alignment even if using the same k-mer based index structure.
Since compatibility of both programs is not trivial, as for instance PriLive does
not support gapped k-mers, it is not automatically merged when a new HiLive version
is published. Instead, the underlying HiLive version of PriLive is only updated in
irregular intervals. Therefore, we strongly recommend to download HiLive separately
if the real-time filtering functionality of PriLive is not required.

For PriLive 0.1, the underlying HiLive version is HiLive 0.3.

---

#### Manual

PriLive comes with a manual file that can be opened using 

	man ./prilive.man
---

License
-------

See the file LICENSE or type

	prilive --license

for licensing information.


Contact
-------

Please consult the PriLive project website for questions!

If this does not help, please feel free to consult:

 * Tobias P. Loka <tobias.loka (at) posteo.de> (technical contact)
 * Bernhard Y. Renard <renardb (at) rki.de> (project head)

also see CONTRIBUTORS for a complete list of contributors and their contact
information.
