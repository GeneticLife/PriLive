#!/bin/bash

help="Get flanking regions of y-STR markers from the lobSTR index.\n\nREQUIRED PARAMETERS:\n\t-f\tInput FASTA file (contains nucleotide sequences)\n\t-b\tInput BED file (contains list of markers and their start/end position)\n\t-o\tOutput file (.fa or .fasta)\n\nGENERAL PARAMETERS:\n\t-h\tPrint help\n"
filter="^";

while getopts f:b:o:h opt
do
    case $opt in
	f) fasta=$OPTARG;;
	o) output=$OPTARG;;
	b) bed=$OPTARG;;
	h) echo -e $help; exit 0;
    esac
done

if [ -z $fasta ]; then
    echo "Please specify an input fasta file (type 'get_flanking_ystr_regions.sh -h' for help)"
    exit 1;
elif [ -z $output ]; then
    echo "Please specify an output file (type 'get_flanking_ystr_regions.sh -h' for help)"
    exit 1;
elif [ -z $bed ]; then
    echo "Please specify an input bed file (type 'get_flanking_ystr_regions.sh -h' for help)"
    exit 1;
fi
	
# IMPORTANT: THE LEADING AND TRAILLING Ns MUST BE REMOVED IN THE INPUT FASTA FILE, e.g. via:
sed 's/^N*//;s/N*$//g' $fasta > "$fasta.split"

# Iterate through the bed files containing the YSTR positions
while read line; do

      # store fields from bed file
      header=$(cut -f6 <(echo "$line"));
      start=$(cut -f2 <(echo "$line"));
      end=$(cut -f3 <(echo "$line"));

      # give fields to awk
      awk -vstart=$start -vend=$end -vheader=$header -F '$' '

      	  # define maximal length of the flank
      	  BEGIN{
		flanking_len=100;
	  }

	  # check positions in header of the ySTR file
	  NR%2==1{
		if(start >= $3 && start <= $4){
			 printSeq=1;
			 strStart=start-$3;
			 strEnd=end-$3;
			 header=">"header;
		}
	  }
      	  NR%2==0{
		if(printSeq == 1){
			    print header"/l";
			    if ( strStart>=flanking_len ){
			       seqStart=strStart-flanking_len;
			    }
			    else{
				seqStart=0
			    };
			    print substr($0,seqStart,strStart-seqStart);
			    print header"/r";
			    print substr($0,strEnd+1,flanking_len);
			    printSeq=0;
		}
	}' "$fasta.split" ;
done < $bed > $output
