#!/usr/bin/python

import sys
import os
import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("Ref", help="File containing paths to reference files.")
    parser.add_argument("-l", "--length", help="Length of the sampled reads.", 
                        default=100)
    parser.add_argument("-o", "--output", help="Output file name for the .fastq file.",
                        required=True)
    parser.add_argument("-p", "--paired", help="Second output file name for simulating paired end reads.", default="")
    parser.add_argument("-n", "--number", help="Number of reads being simulated",
                        default=100000)
    parser.add_argument("-b", "--bam", help="Add alignment output in BAM format", default="")
    parser.add_argument("-f", "--factor", help="Average error percentage factor. Please pay attention to the fact that this factor neglects increasing error probability in illumina sequencing procedures.", default=1.0)
    parser.add_argument("-t", "--threads", help="Number of threads used by mason", default=1)
    parser.add_argument("-N", "--name", help="Name of the reads", default="simulated")
    parser.add_argument("-s", "--seed", help="Seed to use for random number generator of mason", default=0)

    args = parser.parse_args()
        
    return args

def main(argv):
    args = parse_args()

    files = [line.strip().split() for line in open(args.Ref).readlines()]

    outfile = open(args.output, "w")

    factor = float(args.factor)
    
    indelPerc = 0.00125*factor;
    snpPerc = 0.01*factor;

    if args.bam[-4:]==".bam":
        bamcommand = "-oa %s" %(args.bam);
    else:
        bamcommand = "";

    if args.paired[-6:]==".fastq" or args.paired[-3:]==".fq":
        pairedcommand = "-or tmp2.fastq";
    else:
        pairedcommand = "";
        
    for f in files[:10]:
        #TODO: Does not work if reference file folder is read only
        command = "mason -ir %s --seed %s --illumina-read-length %s --illumina-prob-insert %s --illumina-prob-deletion %s --illumina-prob-mismatch %s --illumina-prob-mismatch-begin %s --illumina-prob-mismatch-end %s -n %s --num-threads %s --read-name-prefix %s %s -o %s %s" \
                    %(f[0], args.seed, args.length, indelPerc, indelPerc, snpPerc, snpPerc, snpPerc, args.number, args.threads, args.name, bamcommand, "tmp.fastq", pairedcommand)
        print command
        os.system(command)

        with open("tmp.fastq") as tmp_in:
            for line in tmp_in:
                outfile.write(line)

        os.remove("tmp.fastq")
                
        if pairedcommand != "":
            outfile = open(args.paired, "w")
            with open("tmp2.fastq") as tmp_in:
                for line in tmp_in:
                    outfile.write(line);
            os.remove("tmp2.fastq")

if __name__=="__main__":
    main(sys.argv)
