#!/bin/bash

help="Decrypt a BaseCalls directory of Illumina HiSeq sequencing that was encrypted by PriLive v0.1\n\nUSAGE:\n\tdecrypt_bcl_copies.sh \$BCLDIR \$SESSIONKEY \$SESSIONIV \$PRIVATEKEY\n\nPOSITIONAL PARAMETERS:\n\t\$BCLDIR\tPath to the copied BaseCall directory\n\t\$SESSIONKEY\tPath to the session key (by default located in \$BCLDIR)\n\t\$SESSIONIV\tPath to the session iv (by deefault located in \$BCLDIR)\n\t\$PRIVATEKEY\tPath to the private key\n\nGENERAL PARAMETERS:\n\t-h\tPrint help\n"

while getopts h opt
do
    case $opt in
	h) echo -e $help; exit 0;;
    esac
done

if [ "$#" -ne 4 ]; then
    echo "Wrong number of parameters. Type 'reproduce_illumina_run.sh -h' for help."
    exit 1;
fi

bclPath=$1
sessionKeyPath=$2
sessionIvPath=$3
privateKeyPath=$4

if [ ! -f $sessionKeyPath ]
then
    echo "Session key $sessionKeyPath is no file."
    exit 1;
fi

if [ ! -f $sessionIvPath ]
then
    echo "Session initialization vector $sessionIvPath is no file."
    exit 1;
fi

if [ ! -f $privateKeyPath ]
then
    echo "Private key $privateKeyPath is no file."
    exit 1;
fi

if [ ! -d $bclPath ]
then
    echo "Path $bclPath does not exist. Exit."
    exit 1;
fi

exitStatus=0;

SESSIONIV=$(hexdump -ve '1/1 "%.2x"' $sessionIvPath)
#echo $SESSIONIV

openssl rsautl -decrypt -inkey $privateKeyPath -in $sessionKeyPath -out $sessionKeyPath.dec
SESSIONKEY=$(hexdump -ve '1/1 "%.2x"' $sessionKeyPath.dec)
#echo $SESSIONKEY
rm $sessionKeyPath.dec

FILES=$(find $bclPath -type f -name '*.bcl.enc')

successCounter=0
noSuccessCounter=0

for f in $FILES
do
    #COMMAND="openssl enc -d -aes-256-cbc -iv $SESSIONIV -K $SESSIONKEY -in $f -out $f.dec"
    #echo $COMMAND
    fDec="${f/.bcl.enc/.bcl}"
    openssl enc -d -aes-256-cbc -iv $SESSIONIV -K $SESSIONKEY -in $f -out $fDec
    if [ $? != 0 ]; then
	echo "Error: Decrypting file $f did not succeed";
	noSuccessCounter=$((noSuccessCounter+1))
    else
	successCounter=$((successCounter+1))
    fi
done
echo "Decryption of $successCounter files successful."
echo "Decryption of $noSuccessCounter files failed."

unset SESSIONKEY
unset SESSIONIV
unset privateKeyPath

exit 0;
