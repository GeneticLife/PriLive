#!/bin/bash

help="Create a Y-Search link from a .tsv file that contains a Y-STR genotype produced by lobSTR.\n\nUSAGE:\n\tget_ysearch_link.sh \$INPUT\n\nPOSITIONAL PARAMETERS:\n\t\$INPUT\tInput file (.tsv)\n\nGENERAL PARAMETERS:\n\t-h\tPrint help\n"

while getopts h opt
do
    case $opt in
	h) echo -e $help; exit 0;;
    esac
done

input=$1
link_start="http://www.ysearch.org/search_search.asp?fail=1&uid=&freeentry=true&"
link_end="min_markers=8&mismatch_type=absolute&mismatches_max=1&mismatches_sliding_starting_marker=8"

link=$link_start

ystrs=("DYS393" "DYS390" "DYS394" "NULL" "DYS391" "NULL" "NULL" "DYS426" "DYS388" "DYS439" "DYS389I" "DYS392" "NULL" "DYS458" "NULL" "NULL" "DYS455" "DYS454" "DYS447" "DYS437" "DYS448" "DYS449" "NULL" "NULL" "NULL" "NULL" "NULL" "NULL" "NULL" "DYS460" "GATA-H4" "NULL" "NULL" "DYS456" "DYS607" "DYS576" "DYS570" "NULL" "NULL" "DYS442" "DYS438" "NULL" "DYS461" "DYS462" "GATA-A10" "DYS635" "NULL" "DYS441" "DYS444" "DYS445" "DYS446" "DYS452" "DYS463" "DYS531" "DYS578" "NULL" "NULL" "DYS590" "DYS537" "DYS641" "DYS472" "DYS406S1" "DYS511" "NULL" "NULL" "DYS557" "DYS594" "DYS436" "DYS490" "DYS534" "DYS450" "DYS481" "DYS520" "DYS617" "DYS568" "DYS487" "DYS572" "NULL" "DYS492" "DYS565" "DYS434" "DYS435" "DYS485" "DYS494" "DYS495" "DYS505" "DYS522" "DYS533" "DYS549" "DYS556" "DYS575" "DYS589" "DYS636" "DYS638" "DYS643" "DYS714" "DYS716" "DYS717" "DYS726" "NULL");


for i in $(seq -s ' ' 1 100); do
    link_part="L${i}=";
    haplotype=$(grep -P "${ystrs[$i-1]}\t" $input | awk '{ if ( $8 ~ /^[0-9]+$/ ) {print $8}}' | awk '{s+=$1}END{print s}')

    if [[ $haplotype != "" ]] && [[ ${ystrs[$i-1]} == "DYS442" ]]; then
	haplotype=$(($haplotype-5))
    fi

    if [[ $haplotype != "" ]]; then
	link="$link$link_part$haplotype&"
    fi
    
done

echo "$link$link_end"
