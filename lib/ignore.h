#ifndef IGNORE_H
#define IGNORE_H

#include "headers.h"
#include "tools.h"
#include <tr1/unordered_map>
#include "illumina_parsers.h"

/**
 * Describes the status of a read in the PriLive workflow.
 * @author Tobias Loka
 */
enum class IgnoreStatus : uint8_t {

	HANDLED = 0, 			// 000 : RETIRED algorithm started and not (yet) fulfills discard criteria in foreground alignment.
	PRE_DISCARDED = 1, 		// 001 : Detected by RETIRED algorithm, not (yet) fulfills discard criteria in foreground alignment.
	ALLOW_DISCARDING = 2,	// 010 : RETIRED algorithm started, fulfills discard criteria in foreground alignment.
	NEW_DISCARDED = 3, 		// 011 : discarded, initial bcl file manipulation not done yet
	DISCARDED = 4, 			// 100 : discarded and initial bcl file manipulation already done
	DONT_HANDLE = 5,		// 101 : will never be handled (e.g. due to a wrong barcode)
	WRONG_BARCODE = 6,		// 110 : Read has not the correct barcode.
	UNHANDLED = 7, 			// % : not handled in ignore alignment yet
	PAUSED = 8				// % : seed creation in ignore alignment is paused. Besides that, behaves like and is saved as HANDLED.

};

/**
 * Map describing the IgnoreStatus for each read of the handled task.
 * key<uint32_t>: The read number (0-based; incrementing)
 * value<IgnoreStatus>: The IgnoreStatus of the respective read
 * @author Tobias Loka
 */
typedef std::tr1::unordered_map<uint32_t, IgnoreStatus> IgnoreMap;

/**
 * Class to organize everything with respect to the IgnoreStatus of reads.
 * This includes:
 * - Functions to check the status criteria
 * - Storing the status of reads (I/O, file handling, internal data structur)
 * - Manipulation of the bcl files when a read is discarded
 * - Stats file for the ignore status
 * @author Tobias Loka
 */
class IgnoreHandler{
private:

	/** The base directory of the sequencing run */
	std::string root;

	/** The cycle of the current read / mate */
	uint16_t cycle;

	/** The mate number */
	uint16_t mate;

	/** The lane number */
	uint16_t lane;

	/** The tile number */
	uint16_t tile;

	/** The Alignment settings */
	AlignmentSettings* settings;

	/** Number of reads ignored in the respective lane/tile combination */
	uint32_t num_ignored_reads;

	/** Number of bytes at the beginning of the BCL file before the basecalls start */
	const uint8_t BCL_OFFSET = 4;

	/** Value of the "deleted" byte */
	const uint8_t NULLBYTE = 0;

	/** Value if the IgnoreStatus of a read cannot be (de-)serialized */
	const uint32_t INVALID = std::numeric_limits<uint32_t>::max();

	/** List of reads that changed to discarded status since the last bcl manipulation */
	std::vector<uint32_t> newly_discarded_reads;

	/** BclParser to use the loaded basecalls from the main alignment without additional I/O. */
	BclParser basecalls;

	/** HashMap containing the handled reads. Keep private to prevent that external parts do stupid things. */
	IgnoreMap considered_reads_map;

	/**
	 * Load the ignored and discarded reads from the ignore file.
	 * @author Tobias Loka
	 */
	void load_ignored_reads();

	/**
	 * Set the HashMap entry with read number and ignore status.
	 * @param read The read number to change the status.
	 * @param stat The new read status.
	 * @author Tobias Loka
	 */
	inline void setIgnoreStatus(uint32_t read, IgnoreStatus stat){ considered_reads_map[read] = stat; };

	/**
	 * Check whether the minimal number of mismatches in reference alignment fulfills the criteria to start ignore alignment or not.
	 * @param min_errors The minimal number of errors in the foreground alignment.
	 * @return true, if handle criteria are fulfilled. false if not.
	 * @author Tobias Loka
	 */
	inline bool checkHandleCriteria(CountType min_errors){

		// K not reached
		if(cycle <= K_PriLive || settings->bg_min_errors_start == NOT_ACTIVATED)
			return false;

		if( settings->bg_min_errors_start <= min_errors ) {
			return true;
		}
		return false;
	}

	/**
	 * Check whether or not the minimal number of mismatches in reference alignment fulfills the criteria to discard a read if ignore alignment matches.
	 * @param min_errors The minimal number of errors in the foreground alignment.
	 * @return true, if discard criteria are fulfilled. false if not.
	 * @author Tobias Loka
	 */
	inline bool checkAllowDiscardingCriteria(CountType min_errors) {

		// Don't allow before first k-mer
		if ( cycle < settings->kmer_span )
			return false;

		return min_errors >= getCurrentErrorsDiscard();

//		// Beginning of dynamics not reached yet -> take lower bound
//		if ( cycle < settings->ign_dynamic_min_cycle )
//			return min_errors >= settings->ign_min_errors_discard;
//
//		// End of dynamics reached -> take upper bound
//		if ( cycle >= settings->ign_dynamic_max_cycle )
//			return min_errors >= settings->ign_max_errors_discard;
//
//		float factor = ( (float) (cycle - settings->ign_dynamic_min_cycle) / (settings->ign_dynamic_max_cycle - settings->ign_dynamic_min_cycle));
//
//		return ( min_errors >= (CountType) (settings->ign_min_errors_discard + ( (settings->ign_max_errors_discard - settings->ign_min_errors_discard) * factor) ));
	}

	/**
	 * Get the minimal number of errors in fg alignment that is currently required to discard a read.
	 * @return The required number of errors to discard a read.
	 * @author Tobias Loka
	 */
	inline CountType getCurrentErrorsDiscard( ) {

		if ( cycle < settings->bg_dynamic_min_cycle )
			return settings->bg_min_errors_discard;

		if ( cycle >= settings->bg_dynamic_max_cycle )
			return settings->bg_max_errors_discard;

		float factor = ( (float) (cycle - settings->bg_dynamic_min_cycle) / (settings->bg_dynamic_max_cycle - settings->bg_dynamic_min_cycle));

		return (CountType) (settings->bg_min_errors_discard + ( (settings->bg_max_errors_discard - settings->bg_min_errors_discard) * factor) );
	}

	/**
	 * Check whether seed creation in ignore alignment is to be paused. This happens, if the foreground alignment becomes better than expected
	 * at an earlier point in time.
	 * @param min_errors The minimal number of errors in the foreground alignment.
	 * @return true, if the PriLive seed creation is to be paused. false if not.
	 * @author Tobias Loka
	 */
	inline bool checkPauseCriteria(CountType min_errors){

		// if handle criteria not fulfilled, pause!
		if( ! checkHandleCriteria(min_errors) )
			return true;

		// if discarding allowed, don't pause!
		if ( checkAllowDiscardingCriteria(min_errors) )
			return false;

		return false;
	}

public:

	/**
	 * Class constructor of a new IgnoreHandler. Must be initialized at least once for each task.
	 * @param ln The respective lane number.
	 * @param tl The respective tile number.
	 * @param cyc The respective read cycle. This does not necessarily equal the sequencing cycle. If using barcodes and/or several mates,
	 * the cycle of the current mate must be declared here.
	 * @param mt The current mate number (1-based; e.g. 1 or 2 in standard paired-end sequencing.)
	 * @param rt The base directory of the sequencing run.
	 * @param al_settings Pointer to an object containing the program settings.
	 * @param loadReads True, if you want to load the existing file containing the IgnoreStatus information.
	 * Should only be false during initialization. [optional / default: true]
	 * @author Tobias Loka
	 */
	IgnoreHandler(uint16_t ln, uint16_t tl, uint16_t cyc, uint16_t mt, std::string rt, AlignmentSettings* al_settings, bool loadReads = true):
		root(rt), cycle(cyc), mate(mt), lane(ln), tile(tl), settings(al_settings), num_ignored_reads(0) {
		if(loadReads)
			load_ignored_reads();
	}

	// ---------- File handling ----------

	/**
	 * Get the path for the respective ignore file.
	 * @param base If different base location than root, specify here.
	 * @return The full path of the file containing the IgnoreStatus for the respective task.
	 * @author Tobias Loka
	 */
	std::string get_ignore_file(std::string base="");
	std::string get_ignore_file(uint16_t mt, std::string base="");

	/**
	 * Get path for the respective names file containing the names for all reads.
	 * @return The full path of the file containing the read names.
	 * @author Tobias Loka
	 */
	std::string get_names_file();

	/**
	 * Get path for the file containing the read names of discarded reads.
	 * @param base If different base location than root, specify here.
	 * @return The full path of the file containing the read names of discarded reads.
	 * @author Tobias Loka
	 */
	std::string get_cont_file(std::string base="");

	/**
	 * Init necessary initial files for the ignore alignment (.ia, .ignore).
	 * Should only be called once for each tile (before doing any alignment stuff)
	 * @author Tobias Loka
	 */
	void init_ignore_file();

	/**
	 * Init file containing the names of reads discarded by PriLive.
	 * @return true, if successful. False, if errors occured.
	 * @author Tobias Loka
	 */
	bool init_cont_file();

	/**
	 * Add reads to contamination file. All reads are added without further controls, so it must be ensured before that the given
	 * reads are not already listed in the file.
	 * @param reads List of reads to be added to the contamination file.
	 * @author Tobias Loka
	 */
	void add_cont_reads(std::vector<uint32_t>& reads);

	/**
	 * Save Ignore Status of considered reads in ignore file. Also organizes the deletion of bases from the bcl files.
	 * @param bcl_manip true, if bcl files will be manipulated [optional / default: false]
	 * @author Tobias Loka
	 */
	void update_ignore_file(bool is_discard_cycle, bool bcl_manip);

	/**
	 * Serialize the IgnoreStatus information for efficiently storing it in the ignore file.
	 * @param deserialized_read Number of the read in deserialized format.
	 * @param status The IgnoreStatus of the respective read.
	 * @return The serialized value containing read number and status.
	 * @author Tobias Loka
	 */
	uint32_t serialize_read(uint32_t deserialized_read, IgnoreStatus status);

	/**
	 * Deserialize a stored read to split the information into read number and status.
	 * @param serialized_read The serialized read how it was stored in the ignore file.
	 * @return A pair containing the read number and the status.
	 * @author Tobias Loka
	 */
	std::pair<uint32_t, IgnoreStatus> deserialize_read(uint32_t serialized_read);


	// ---------- Error criteria ----------

	/**
	 * Calculate background alignment score that is necessary to discard a read when dynamic scores are activated.
	 * @return The calculated score of the background alignment that is necessary to discard a read.
	 * @author Tobias Loka
	 */
	inline CountType getDynamicIgnoreScore() {

		CountType min_c = settings->bg_dynamic_min_cycle;
		CountType max_c = settings->bg_dynamic_max_cycle;

		// lower boundary not reached
		if(cycle < min_c)
			return std::numeric_limits<CountType>::max();

		// Lower and upper boundary equal, return maximum score
		if ( min_c == max_c )
			return settings->bg_max_score;

		// Behind last dynamic cycle, return maximum
		if(cycle >= max_c)
			return settings->bg_max_score;

		float factor = (float) (cycle - min_c) / (max_c - min_c);
		return ( settings->bg_min_score + ( (float) (settings->bg_max_score - settings->bg_min_score) * factor) );
	}

	/**
	 * Get information from foreground alignment and change the ignore status respectively.
	 * @param read The number of the respective read.
	 * @param min_errors The minimal number of errors in the best foreground alignment.
	 * @return The new ignore status
	 * @author Tobias Loka
	 */
	IgnoreStatus updateForegroundAlignmentStatus(uint32_t read, CountType min_errors);

	/**
	 * Get information from background alignment and change the ignore status respectively.
	 * @param read The number of the respective read.
	 * @param max_num_matches The score of the best background alignment.
	 * @return The new ignore status
	 * @author Tobias Loka
	 */
	IgnoreStatus updateBackgroundAlignmentStatus(uint32_t read, CountType max_num_matches);

	 // ---------- BCL file manipulation ----------

	/**
	 * Anonymize base calls of a set of reads in the bcl files of a certain cycle.
	 * @param reads The list of reads to anonymize.
	 * @param cycle The cycle number to anonymize (the sequencing cycle is required here, NOT the read cycle!)
	 * @author Tobias Loka
	 */
	void removeFromBcl(std::vector<uint32_t>& reads, uint16_t cycle);

	/**
	 * Anonymize base calls of a set of reads in the bcl files of a certain span of cycles.
	 * @param reads The list of reads to anonymize.
	 * @param first_cycle The first cycle number to anonymize (the sequencing cycle is required here, NOT the read cycle!)
	 * @param last_cycle The last cycle number to anonymize (the sequencing cycle is required here, NOT the read cycle!)
	 * @author Tobias Loka
	 */
	void removeFromBcl(std::vector<uint32_t>& reads, uint16_t first_cycle, uint16_t last_cycle);

	// ---------- Ignored Reads HashMap functions ----------

	/**
	 * Check whether any read has a different status than UNHANDLED.
	 * @return true, if there exist reads that have a status different to UNHANDLED. false if not.
	 * @author Tobias Loka
	 */
	inline bool hasHandledReads(){ return considered_reads_map.size()>0; };

	// ---------- IgnoreStatus setters ----------

	/**
	 * Change ignore status of a certain read to HANDLED.
	 * @param read The number of the respective read to be HANDLED.
	 * @author Tobias Loka
	 */
	inline void handleRead(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::HANDLED); };

	/**
	 * Change ignore status of a certain read to ALLOW_DISCARDING.
	 * @param read The number of the respective read to be ALLOW_DISCARDING.
	 * @author Tobias Loka
	 */
	inline void allowDiscardingRead(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::ALLOW_DISCARDING); };

	/**
	 * Change ignore status of a certain read to PRE_DISCARDED.
	 * @param read The number of the respective read to be PRE_DISCARDED.
	 * @author Tobias Loka
	 */
	inline void preDiscardRead(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::PRE_DISCARDED); };

	/**
	 * Change ignore status of a certain read to NEW_DISCARDED.
	 * @param read The number of the respective read to be NEW_DISCARDED.
	 * @author Tobias Loka
	 */
	inline void discardRead(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::NEW_DISCARDED); };

	/**
	 * Change ignore status of a certain read to PAUSED.
	 * @param read The number of the respective read to be PAUSED.
	 * @author Tobias Loka
	 */
	inline void pauseRead(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::PAUSED); };

	/**
	 * Change ignore status of a certain read to DONT_HANDLE.
	 * @param read The number of the respective read to be DONT_HANDLE.
	 * @author Tobias Loka
	 */
	inline void dontHandle(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::DONT_HANDLE); };

	inline void wrongBarcode(uint32_t read) { setIgnoreStatus(read, IgnoreStatus::WRONG_BARCODE); };

	 // ---------- IgnoreStatus getters ----------

	/**
	 * Check whether a certain read is HANDLED or not.
	 * @param read The read number to check for status HANDLED.
	 * @return true, if read has status HANDLED. false if not.
	 * @author Tobias Loka
	 */
	inline bool isHandled(uint32_t read){return ( getIgnoreStatus(read)==IgnoreStatus::HANDLED || getIgnoreStatus(read)==IgnoreStatus::PAUSED );};

	/**
	 * Check whether a certain read is ALLOW_DISCARDING or not.
	 * @param read The read number to check for status ALLOW_DISCARDING.
	 * @return true, if read has status ALLOW_DISCARDING. false if not.
	 * @author Tobias Loka
	 */
	inline bool isAllowDiscarding(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::ALLOW_DISCARDING;};

	/**
	 * Check whether a certain read is PRE_DISCARDED or not.
	 * @param read The read number to check for status PRE_DISCARDED.
	 * @return true, if read has status PRE_DISCARDED. false if not.
	 * @author Tobias Loka
	 */
	inline bool isPreDiscarded(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::PRE_DISCARDED;};

	/**
	 * Check whether a certain read is NEW_DISCARDED or not.
	 * @param read The read number to check for status NEW_DISCARDED.
	 * @return true, if read has status NEW_DISCARDED. false if not.
	 * @author Tobias Loka
	 */
	inline bool isNewDiscarded(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::NEW_DISCARDED;};

	/**
	 * Check whether a certain read is DISCARDED or not.
	 * @param read The read number to check for status DISCARDED.
	 * @return true, if read has status DISCARDED. false if not.
	 * @author Tobias Loka
	 */
	inline bool isDiscarded(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::DISCARDED;};

	/**
	 * Check whether a certain read is UNHANDLED or not.
	 * @param read The read number to check for status UNHANDLED.
	 * @return true, if read has status UNHANDLED. false if not.
	 * @author Tobias Loka
	 */
	inline bool isUnhandled(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::UNHANDLED;};

	/**
	 * Check whether a certain read is PAUSED or not.
	 * @param read The read number to check for status PAUSED.
	 * @return true, if read has status PAUSED. false if not.
	 * @author Tobias Loka
	 */
	inline bool isPaused(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::PAUSED;};

	/**
	 * Check whether a certain read is DONT_HANDLE or not.
	 * @param read The read number to check for status DONT_HANDLE.
	 * @return true, if read has status DONT_HANDLE. false if not.
	 * @author Tobias Loka
	 */
	inline bool isDontHandle(uint32_t read){return getIgnoreStatus(read)==IgnoreStatus::DONT_HANDLE || getIgnoreStatus(read)==IgnoreStatus::WRONG_BARCODE;};

	/**
	 * Get the current ignore status of a certain read.
	 * @param read The read number to get the status.
	 * @return The current ignore status of the respective read.
	 * @author Tobias Loka
	 */
	inline IgnoreStatus getIgnoreStatus(uint32_t read){

		if ( !settings->hasIgnoreAlignment() )
			return IgnoreStatus::UNHANDLED;

		auto findResult = considered_reads_map.find(read);

		/* if read is not in list, it's unhandled */
		if(findResult == considered_reads_map.end())
			return IgnoreStatus::UNHANDLED;

		return findResult->second;
	}

	/**
	 * Check whether or not a certain read has a status that implies that the read is considered in the background alignment.
	 * @param read The read number to check for background alignment consideration.
	 * @param true, if the respective read is considered in the background alignment. false if not.
	 * @author Tobias Loka
	 */
	inline bool isInIgnoreAlignment(uint32_t read){if(!(settings->hasIgnoreAlignment())) return false; return ( isHandled(read) || isAllowDiscarding(read) ); };

	/**
	 * Check whether or not a certain read has a status that implies that the read is considered in the foreground alignment.
	 * @param read The read number to check for foreground alignment consideration.
	 * @param true, if the respective read is considered in the foreground alignment. false if not.
	 * @author Tobias Loka
	 */
	inline bool isInMainAlignment(uint32_t read){if(!(settings->hasIgnoreAlignment())) return true; return ( !(isNewDiscarded(read) || isDiscarded(read)) );}

	/**
	 * Set the BclParser that is stored to avoid a second load of all bcl files for the background alignment.
	 * @param bcl Pointer to the BclParser object containing the respective bcl file information.
	 * @author Tobias Loka
	 */
	inline void setBasecalls(BclParser* bcl){basecalls=*bcl;};

	/**
	 * Get the BclParser that is stored to avoid a second load of all bcl files for the background alignment.
	 * @return Pointer to the BclParser object containing the respective bcl file information.
	 * @author Tobias Loka
	 */
	inline BclParser* getBasecalls(){return &basecalls;};
};

#endif /* IGNORE_REF_H */
