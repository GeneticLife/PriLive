#include "argument_parser.h"
namespace po = boost::program_options;


int parseCommandLineArguments(AlignmentSettings & settings, std::string license, int argc, char const ** argv)
{
    po::options_description general("General");
    general.add_options()
        ("help,h", "Print this help message and exit")
		("license", "Print licensing information and exit")
    	("accept-disclaimer", "Confirm that you read, understood and accept the disclaimer of PriLive (type \'prilive --license\' to read)");


    po::options_description parameters("Parameters");
    parameters.add_options()
        ("BC_DIR", po::value<std::string>(&settings.root)->required(), "Illumina BaseCalls directory")
        ("INDEX", po::value<std::string>(&settings.index_fname)->required(), "Path to foreground index")
        ("CYCLES", po::value<CountType>(&settings.cycles)->required(), "Number of cycles")
        ("OUTDIR", po::value<std::string>(&settings.out_dir), "Directory to store output files [Default: temporary or BaseCalls directory");

    po::options_description seq_settings("Sequencing setup");
    seq_settings.add_options()
		("lanes,l", po::value< std::vector<uint16_t> >()->multitoken()->composing(), "Considered lanes [Default: all lanes]")
		("tiles,t", po::value< std::vector<uint16_t> >()->multitoken()->composing(), "Considered tiles [Default: all tiles]")
		("reads,r", po::value< std::vector<std::string> >()->multitoken()->composing(), "Define read fragments, e.g. '101R 8B 8B 101R' for 2x101bp reads and 2x8bp barcodes.")
		("barcodes,b", po::value< std::vector<std::string> >()->multitoken()->composing(), "Barcode sequence(s) [Default: no demultiplexing]")
		("barcode-errors,E", po::value< std::vector<uint16_t> >()->multitoken()->composing(), "Tolerated errors for barcode fragments [Default: 1 per fragment]")
		("keep-all-barcodes", po::bool_switch(&settings.keep_all_barcodes)->default_value(false), "Filter and align all reads [Default: false]");

    po::options_description io_settings("I/O settings");
    io_settings.add_options()
        ("temp", po::value<std::string>(&settings.temp_dir)->default_value(""), "Directory for temporary files [Default: BC_DIR]")
        //("bam,B", po::bool_switch(&settings.write_bam)->default_value(false), "Create BAM files instead of SAM files [Default: false]")
        ("keep-files,k", po::bool_switch(&settings.keep_aln_files)->default_value(false), "Keep intermediate alignment files [Default: false]")
		("copy", po::value<std::string>(), "Copy directory for original .bcl files [Default: none]")
		("copy-enc", po::value<std::string>(), "Encryption for bcl copies. plain or ssl [Default: ssl]")
		("pub-key", po::value<std::string>(), "Path to the public key for copy encryption (.pem)");

    po::options_description alignment("Foreground alignment settings");
    alignment.add_options()
        ("min-errors,e", po::value<CountType>(&settings.min_errors)->default_value(2), "Number of errors tolerated in foreground alignment [Default: 2]")
        ("all-best-hit,H", po::bool_switch()->default_value(false), "Report all best alignments")
        ("any-best-hit", po::bool_switch(), "Report one best alignment (default)")
        ("all-best-n-scores,N", po::value<CountType>(&settings.best_n), "Report all alignments of the N best scores")
        ("all-hits,A", po::bool_switch()->default_value(false), "Report all alignments")
        ("disable-ohw-filter", po::bool_switch(&settings.discard_ohw)->default_value(true), "Disable the One-Hit Wonder filter [Default: false]")
        ("start-ohw", po::value<CountType>(&settings.start_ohw)->default_value(K_PriLive+5), "First cycle to apply One-Hit Wonder filter [Default: K+5]")
        ("window,w", po::value<DiffType>(&settings.window)->default_value(5), "Window size for alignment extension [Default: 5]")
        ("min-quality", po::value<CountType>(&settings.min_qual)->default_value(1), "Minimum valid basecall quality [Default: 1]");

    po::options_description ign_alignment("Background alignment / Read filtering settings");
      ign_alignment.add_options()
    	("bg,f", po::value<std::string>(), "Path to background index.")
    	("bg-score", po::value<std::vector<CountType>>()->multitoken()->composing(), "Minimal background alignment score for read filtering [default: auto]")
    	("bg-anchor", po::value<CountType>(&settings.bg_min_anchor_length)->default_value(K_PriLive+4), "Length of the anchor [default: K+4]")
    	("bg-islands", po::value<CountType>(&settings.bg_min_island_length)->default_value(3), "Minimum number of matches between two mismatches (must be >=1) [default: 3]")
    	("bg-start", po::value<CountType>(&settings.bg_min_errors_start), "Minimum errors in foreground alignment to start background alignment [default: auto]")
    	("bg-discard", po::value<std::vector<CountType>>()->multitoken()->composing(), "Minimum errors in foreground alignment to allow filtering [default: auto]")
    	("bg-freq", po::value<uint16_t>(&settings.bg_discard_frequency)->default_value(10), "Interval to filter from bcl files (0: Don't remove sequences) [default: 10]")
		("bg-dynamic-cycles", po::value<std::vector<uint16_t>>()->multitoken()->composing(), "Cycles for dynamic parameter increase [default: automatic]")
		("delay", po::value<CountType>(&settings.cycleDelay)->default_value(0), "Delay for filtering from bcl files [default: 0]")
		("bg-min-barcodes", po::value<uint16_t>(&settings.bg_barcodes_before_discard), "Number of valid barcodes before filtering from bcl files [default: all]")
		;

    po::options_description technical("Technical settings");
    technical.add_options()
        ("block-size", po::value<uint64_t>()->default_value(64*1024*1024), "Block size for the alignment input/output stream in Bytes. Use -K or -M to specify in Kilobytes or Megabytes")
        (",K", po::bool_switch()->default_value(false), "Interpret the block-size argument as Kilobytes instead of Bytes")
        (",M", po::bool_switch()->default_value(false), "Interpret the block-size argument as Megabytes instead of Bytes")
        ("compression,c", po::value<uint8_t>(&settings.compression_format)->default_value(2), "Compress alignment files. 0: no compression 1: Deflate (smaller) 2: LZ4 (faster; default)")
        ("num-threads,n", po::value<int>(), "Number of threads to spawn [Default: all available]");

    po::options_description debug("Debug & Beta Settings");
    debug.add_options()
		("decreasing-delay", po::bool_switch(&settings.decreasing_delay)->default_value(false), "Let the delay decrease [Default: false]")
    	("bg-first-filter-cycle", po::value<CountType>(&settings.bg_first_cycle_to_discard)->default_value(1), "First cycle to filter from bcl files [default: 1]")
		("exclude-mates", po::value<std::vector<uint16_t>>(&settings.exclude_mates)->multitoken()->composing(), "Don't handle these mates [Default: handle all]");

    po::options_description cmdline_options;
    cmdline_options.add(general).add(parameters).add(seq_settings).add(io_settings).add(alignment).add(ign_alignment).add(technical).add(debug);

    po::options_description visible_options;
    visible_options.add(general).add(seq_settings).add(io_settings).add(alignment).add(ign_alignment).add(technical);

    std::stringstream help_message;
    help_message << "PriLive v"<< PriLive_VERSION_MAJOR << "." << PriLive_VERSION_MINOR << ": Privacy-preserving real-time filtering of Illumina Reads" << std::endl;
    help_message << "Copyright (c) 2017, Tobias P. Loka & the PriLive contributors. See CONTRIBUTORS for more info." << std::endl;
    help_message << "PriLive is open-source software. Check with --license for details." << std::endl << std::endl;
    help_message << "This software is based on the original HiLive software (Lindner et al., 2017)." << std::endl << std::endl;
    help_message << "Fixed k-mer size: " << K_PriLive << std::endl << std::endl;
    help_message << "Usage: " << std::string(argv[0]) << " [options] BC_DIR INDEX CYCLES OUTDIR" << std::endl << std::endl;
    help_message << "  BC_DIR       Illumina BaseCalls directory of the sequencing run to analyze" << std::endl;
    help_message << "  INDEX        Path to the foreground index file (*.kix)" << std::endl;
    help_message << "  CYCLES       Total number of cycles of the sequencing run" << std::endl;
    help_message << "  OUTDIR       Directory to store output files" << std::endl;

    help_message << visible_options << std::endl;


    std::stringstream disclaimer;
    disclaimer << "DISCLAIMER:" << std::endl;
    disclaimer << "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.";

    po::positional_options_description p;
    p.add("BC_DIR", 1);
    p.add("INDEX", 1);
    p.add("CYCLES", 1);
    p.add("OUTDIR", 1);

    // array for remarks concerning the chosen parameters (for user output)
    std::vector<std::string> remarks;
    
    // parse the arguments
    po::variables_map vm;
    try {

    	po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);

        // check if -h or --help was called
        if (vm.count("help")) {
            std::cout << help_message.str();
            return 1;
        }

        // check if --license was called
        if (vm.count("license")) {
            std::cout << license << std::endl;
            std::cout << std::endl << disclaimer.str() << std::endl;
            return 1;
        }

        // check arguments
        po::notify(vm);  
    }
    catch ( boost::program_options::required_option& e ) {
    	// Obligatry parameter missing
        std::cerr << "Missing Parameter: " << e.what() << std::endl << std::endl;
        std::cout << help_message.str();
        return -1;  
    }
    catch( boost::program_options::error& e) { 
    	// Parsing error
        std::cerr << "Error while parsing command line options: " << e.what() << std::endl << std::endl; 
        std::cout << help_message.str();
        return -1;  
    } 

    // Disclaimer must be accepted explicitely by typing "yes" or as program parameter
    if ( !vm.count("accept-disclaimer") ) {
    	std::string accepted;
    	std::cout << std::endl << disclaimer.str() << std::endl;
    	std::cout << "Please confirm that you read, understood and accept the disclaimer as printed above by typing \"yes\": ";
    	std::cin >> accepted;
    	if ( accepted!="yes" ) {
    		std::cout << "Disclaimer not accepted. Exit program." << std::endl;
    		return 1;
    	}
    }

    // Set output directory
    if (vm.count("OUTDIR"))
        settings.out_dir = vm["OUTDIR"].as<std::string>();
    else {
        if (settings.temp_dir == "") 
            settings.out_dir = settings.root;
        else
            settings.out_dir = settings.temp_dir;
    }

    // Parse read lengths and types. If read argument is missing, init for single-end and non-barcoded.
    if ( vm.count("reads") && !parseReadsArgument(settings, vm["reads"].as< std::vector<std::string> >()) )
    	return -1;
    else if ( !vm.count("reads") ) {
    	settings.seqs.push_back(SequenceElement(0,1,settings.cycles));
    	settings.mates = 1;
    }

    // Set lanes and tiles
    if (vm.count("lanes"))
        settings.lanes = vm["lanes"].as< std::vector<uint16_t> >();
    else
        settings.lanes = all_lanes();

    if (vm.count("tiles"))
        settings.tiles = vm["tiles"].as< std::vector<uint16_t> >();
    else
        settings.tiles = all_tiles();

    // Parse barcodes
    if (vm.count("barcodes")) {
        if( !parseBarcodeArgument(settings, vm["barcodes"].as< std::vector<std::string> >()) ) {
        	std::cerr << "Parsing error: Invalid barcode(s) detected. Please ensure that you used \'-\' "
        			"as duplex delimiter and that all barcodes have the correct length. Only use A,C,G and T as bases!" << std::endl;
        	return -1;
        }
    }

    if ( settings.barcodeVector.size() != 0 ) {
    	if ( vm.count("barcode-errors") ) {
    		settings.barcode_errors = vm["barcode-errors"].as< std::vector<uint16_t> >();
    		if ( settings.barcodeVector[0].size() != settings.barcode_errors.size() ) {
    			std::cerr << "Parsing error: Number of barcode errors does not equal the number of barcodes." << std::endl;
    			return -1;
    		}
    	} else {
        	for ( uint16_t i = 0; i < settings.barcodeVector[0].size(); i++ ) {
        		settings.barcode_errors.push_back(1);
        	}
    	}
    }

    // Set alignment mode
    if (vm["all-hits"].as<bool>()) {
        // all hits: disable other modes
        settings.any_best_hit_mode = false;
        settings.all_best_hit_mode = false;
        settings.all_best_n_scores_mode = false;
    } else if (vm["all-best-hit"].as<bool>()) {
        // enable all-best-hit mode and disable others
        settings.any_best_hit_mode = false;
        settings.all_best_hit_mode = true;
        settings.all_best_n_scores_mode = false;
    } else if (vm.count("all-best-n-scores")) {
        // enable any-best-n mode and get parameter
        settings.any_best_hit_mode = false;
        settings.all_best_hit_mode = false;
        settings.all_best_n_scores_mode = true;
        settings.best_n = vm["all-best-n-scores"].as<CountType>();
    } else { // the default behaviour
        // enable any-best-hit mode and disable others
        settings.any_best_hit_mode = true;
        settings.all_best_hit_mode = false;
        settings.all_best_n_scores_mode = false;
    }

    // Technical settings
    if (vm["-M"].as<bool>())
        settings.block_size = vm["block-size"].as<uint64_t>()*1024*1024;
    else if (vm["-K"].as<bool>())
        settings.block_size = vm["block-size"].as<uint64_t>()*1024;
    else
        settings.block_size = vm["block-size"].as<uint64_t>();

    if (vm.count("num-threads")) 
        settings.num_threads = vm["num-threads"].as<int>();
    else { // the default case, meaning as much as physically useful
        uint32_t n_cpu = std::thread::hardware_concurrency();
        if (n_cpu > 1)
            settings.num_threads = n_cpu - 1;
        else
            settings.num_threads = 1;
    }

    // Settings for bcl file copy
    if (vm.count("copy")){

    	settings.copy_dir = vm["copy"].as<std::string>();

    	boost::filesystem::path dir(settings.copy_dir);
    	if(boost::filesystem::create_directories(dir))
    		remarks.push_back("[REMARK] Directory for Basecall copies created: " + settings.copy_dir);
    	else if(is_directory(settings.copy_dir)){
    		remarks.push_back("[WARNING] Directory for Basecall copies already exist. Existing files will be overwritten!" );
    	} else {
    		settings.copy_dir = "";
    		std::cerr << "----- !!! Directory for Basecall copies could not be created. Stop run !!! -----" << std::endl;
    		return -1;
    	}

    	// Init copy encryption
    	std::string arg = vm.count("copy-enc") ? vm["copy-enc"].as<std::string>() : "ssl";
    	std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);
    	settings.encryptionType = arg=="plain" ? CopyEncryption::PLAIN : CopyEncryption::SSL;

    	// init EVP data structure
    	if(settings.encryptionType==CopyEncryption::SSL){

    		// Ask for path to public key
    		std::string pubKey;
    		if( !vm.count("pub-key") ) {
    			std::cout << "Enter path to public encryption key: " << std::endl;
    			std::cin >> pubKey;
    		} else
    			pubKey = vm["pub-key"].as<std::string>();
    		if ( ! boost::filesystem::exists(pubKey) ) {
    			std::cerr << "Public key file " + pubKey + " for ssl encryption is not accessible. Stop run." << std::endl;
    			return -1;
    		}
    		settings.pubKeyPath = pubKey;

    		// Init SSL encryption
    		init_ssl_encryption(settings);
    	} else {
    		remarks.push_back("[REMARK] You create plain copies of all data. If you perform human read decontamination, we strongly recommend to deactivate copy mode or to use SSL encryption!");
    	}
    }

    // Parameters for host removal parameters
   	if (vm.count("bg")){

   		// Path to index
   		settings.bg_index_fname = vm["bg"].as<std::string>();
   		if (!file_exists(settings.bg_index_fname)){
   			std::cerr << "Input error: Could not find ignore reference index file: " << settings.bg_index_fname << std::endl;
   			return -1;
   		}

   		// Anchor length correction (must be at least the same as k)
   		settings.bg_min_anchor_length = settings.bg_min_anchor_length < K_PriLive ? K_PriLive : settings.bg_min_anchor_length;

   		// Number of errors in foreground alignment to discard a read
   		settings.bg_min_errors_discard = settings.min_errors + 1 - CountType( log2( std::max( CountType(1), settings.min_errors ) ) );
   		settings.bg_max_errors_discard = settings.min_errors + 1;
   		if ( vm.count("bg-discard") ) {
   			auto ign_discard_args = vm["bg-discard"].as< std::vector<uint16_t> >().begin();
   			if ( vm["bg-discard"].as<std::vector<CountType>>().size() == 1 ) {
   	   	   		// static user setting (1 parameter)
   				settings.bg_max_errors_discard = *(ign_discard_args);
   				settings.bg_min_errors_discard = settings.bg_max_errors_discard;
   			} else {
   				// dynamic user setting (2 parameters)
   	   			CountType ign_discard_values[2];
   	   			ign_discard_values[0] = *(ign_discard_args++);
   	   			ign_discard_values[1] = *(ign_discard_args);
   	   			settings.bg_min_errors_discard = std::min(ign_discard_values[0], ign_discard_values[1]);
   	   			settings.bg_max_errors_discard = std::max(ign_discard_values[0], ign_discard_values[1]);

   	   			if ( vm["bg-discard"].as<std::vector<CountType>>().size() > 2 )
   	   				remarks.push_back("[REMARK] Parameter '--bg-discard' processes at most two arguments. Additional parameters were ignored.");
   	   			if ( settings.bg_max_errors_discard >= settings.cycles/settings.kmer_span )
   	   				remarks.push_back("[WARNING] Parameter '--bg-discard' was set too high related to the number of cycles. This may lead to a complete failure of read filtering!");
   	   			if ( settings.bg_min_errors_discard == 0 )
   	   				remarks.push_back("[WARNING] Setting start value for '--bg-discard' to 0 may lead to a filtering of reads that perfectly match the foreground reference!");
   			}
   		}

   		if ( !vm.count("bg-start") )
   				settings.bg_min_errors_start = log2( std::max( CountType(1), settings.min_errors ) );

   		// Host removal alignment score
//   		settings.ign_min_score = std::max ( 30.0, 17 * std::log10(settings.getSeqByMate(1).length) );
//		settings.ign_max_score = settings.ign_min_score * 1.2;
   		settings.bg_min_score = std::max ( 30.0,
   				35 * ( log10( settings.getSeqByMate(1).length - ( (std::max(CountType(1), settings.bg_min_errors_start) - 1) * settings.kmer_span )) -1 ));
   		settings.bg_max_score = settings.bg_min_score;

   		if ( vm.count("bg-score") ) {
   			auto ign_score_args = vm["bg-score"].as< std::vector<uint16_t> >().begin();
   			if ( vm["bg-score"].as<std::vector<CountType>>().size() == 1 ) {
   	   			// static user setting (1 parameter)
   				settings.bg_max_score = *(ign_score_args);
   				settings.bg_min_score = settings.bg_max_score;
   			} else {
   				// dynamic user setting (2 parameters)
   				CountType ign_score_values[2];
   				ign_score_values[0] = *(ign_score_args++);
   				ign_score_values[1] = *(ign_score_args);
   				settings.bg_min_score = std::min(ign_score_values[0], ign_score_values[1]);
   				settings.bg_max_score = std::max(ign_score_values[0], ign_score_values[1]);

   				if ( vm["bg-score"].as<std::vector<CountType>>().size() > 2 )
   					remarks.push_back("[REMARK] Parameter '--bg-score' processes at most two arguments. Additional parameters were ignored.");
   				if ( settings.bg_min_score < 25 )
   					remarks.push_back("[WARNING] Setting start value of '--bg-score' to < 25 may lead to a huge number of erroneously filtered reads due to random matches! Only use lower values for small background reference genomes.");
   			}
   			if ( settings.bg_max_score > settings.cycles- ( settings.kmer_span * settings.bg_min_errors_start ) )
   				remarks.push_back("[WARNING] Parameter '--bg-score' will never be reached in the given number of cycles. This may lead to a complete failure of read filtering!");
   		}


   		// Dynamic parameter settings
   		settings.bg_dynamic_min_cycle = ( std::max( CountType(1), settings.bg_min_errors_start ) * settings.kmer_span ) + 1; 			// cycle when ign alignment can start earliest
   		settings.bg_dynamic_min_cycle += std::max( CountType ( settings.bg_min_score - settings.bg_min_anchor_length ),
   				CountType ( ( settings.bg_min_errors_discard - std::max( CountType(1), settings.bg_min_errors_start ) ) * settings.kmer_span ) ); 	// min. additional cycles to fulfill all discard criteria
   		settings.bg_dynamic_min_cycle = std::max ( settings.bg_min_score , settings.bg_dynamic_min_cycle );							// at least the min. ignore score

   		settings.bg_dynamic_max_cycle = settings.bg_dynamic_min_cycle + std::max( 1, settings.bg_max_errors_discard - settings.bg_min_errors_discard ) * settings.kmer_span;
   		settings.bg_dynamic_max_cycle = std::max ( settings.bg_dynamic_max_cycle, CountType( settings.bg_dynamic_min_cycle + ( 2* (settings.bg_max_score - settings.bg_min_score ) ) ) );
   		settings.bg_dynamic_max_cycle = std::min ( CountType(.8*settings.getSeqByMate(1).length), settings.bg_dynamic_max_cycle );

   		if ( settings.bg_dynamic_max_cycle < settings.bg_dynamic_min_cycle )
   			settings.bg_dynamic_max_cycle = settings.bg_dynamic_min_cycle;

   		if ( vm.count("bg-dynamic-cycles") ) {
   			auto ign_dynamic_cycle_args = vm["bg-dynamic-cycles"].as< std::vector<uint16_t> >().begin();
   			if ( vm["bg-dynamic-cycles"].as< std::vector<uint16_t> >().size() == 1 ) {
   				settings.bg_dynamic_max_cycle = *(ign_dynamic_cycle_args);

   				if ( settings.bg_dynamic_min_cycle >= settings.bg_dynamic_max_cycle )
   					remarks.push_back("[WARNING] The max. dynamic cycle you set is smaller than the calculated min. cycle. Dynamic decontamination will not work with these settings. Decrease ignore Score (--bg-score) or set dynamic start cycle manually.");
   			}
   			else {
   				CountType ign_cycle_values[2];
   				ign_cycle_values[0] = *(ign_dynamic_cycle_args++);
   				ign_cycle_values[1] = *(ign_dynamic_cycle_args);
   				settings.bg_dynamic_min_cycle = std::min(ign_cycle_values[0], ign_cycle_values[1]);
   				settings.bg_dynamic_max_cycle = std::max(ign_cycle_values[0], ign_cycle_values[1]);

   				if ( vm["bg-dynamic-cycles"].as<std::vector<CountType>>().size() > 2 )
   					remarks.push_back("[REMARK] Parameter '--ign-dynmic_cycles' processes at most two arguments. Additional parameters were ignored.");
   			}
   		}

   		// Barcodes that must be available to remove detected reads
   		if ( vm.count("bg-min-barcodes") ) {
   			settings.bg_barcodes_before_discard = std::min( CountType(settings.seqs.size() - settings.mates) , vm["bg-min-barcodes"].as<CountType>() );
   		} else {
   			settings.bg_barcodes_before_discard = settings.seqs.size() - settings.mates;
   		}

   	} else {
   		settings.bg_max_score = NOT_ACTIVATED;
   	}


    // check paths and file names
    if (!file_exists(settings.index_fname)){
        std::cerr << "Input error: Could not find k-mer index file " << settings.index_fname << std::endl;
        return -1;
    }

    std::size_t found = settings.root.find("BaseCalls");
    if (!(found != std::string::npos && found >= settings.root.size()-10)) {
        remarks.push_back("[WARNING] BaseCalls directory seems to be invalid: " + settings.root);
    } 

    if (!is_directory(settings.root)){
        std::cerr << "Input error: Could not find BaseCalls directory " << settings.root << std::endl;
        return -1;
    }

    for ( uint16_t ln : settings.lanes ) {
        std::string ln_dir = settings.root;
        if ( ln < 10 )
            ln_dir += "/L00";
        else if ( ln < 100 )
            ln_dir += "/L0";
        else
            ln_dir += "/L";
        ln_dir += std::to_string(ln);
        if (!is_directory(ln_dir)){
            std::cerr << "Input error: Could not find location of Lane " << ln << ": " << ln_dir << std::endl;
            return -1;
        }
    }


    // Report the basic settings
    std::cout << std::endl;
    std::cout << "Running PriLive with " << settings.num_threads << " thread(s)." << std::endl;
    std::cout << std::endl;
    std::cout << "----- General settings -----" << std::endl;
    std::cout << "BaseCalls directory:       " << settings.root << std::endl;
    if (settings.temp_dir != "") {
        std::cout << "Temporary directory:       " << settings.temp_dir << std::endl;
    }

    //if (!settings.write_bam)
    std::cout << "SAM output directory:      " << settings.out_dir << std::endl;
    //else
        //std::cout << "BAM output directory:     " << settings.out_dir << std::endl;
    std::cout << "Lanes:                     ";

    for ( uint16_t ln : settings.lanes )
        std::cout << ln << " ";
    std::cout << std::endl;
    std::cout << "Foreground index:          " << settings.index_fname << std::endl;
    std::cout << "Read lengths:              ";
    std::string barcode_suffix;
    for ( uint16_t read = 0; read != settings.seqs.size(); read ++) {
    	std::cout << settings.getSeqById(read).length;
    	barcode_suffix = settings.getSeqById(read).isBarcode() ? "B" : "R";
    	std::cout << barcode_suffix << " ";
    }
    std::cout << std::endl;
    std::cout << "Mapping error:             " << settings.min_errors << std::endl;

    if (settings.any_best_hit_mode) 
        std::cout << "Mapping mode:              Any-Best-Hit-Mode" << std::endl;
    else if (settings.all_best_hit_mode) 
        std::cout << "Mapping mode:              All-Best-Hit-Mode" << std::endl;
    else if (settings.all_best_n_scores_mode) 
        std::cout << "Mapping mode:              All-Best-N-Scores-Mode with N=" << settings.best_n << std::endl;
    else
        std::cout << "Mapping mode:              All-Hits-Mode" << std::endl;
    std::cout << std::endl;

    if ( settings.hasIgnoreAlignment() ) {
    	  std::cout << "----- Filtering settings -----" << std::endl;
    	  std::cout << "Background Index:          " << settings.bg_index_fname << std::endl;
    	  std::cout << "Min. anchor length:        " << settings.bg_min_anchor_length << std::endl;
    	  std::cout << "Min. start errors:         " << settings.bg_min_errors_start << std::endl;
    	  std::cout << "Min. discard errors:       " << settings.bg_min_errors_discard << "..." << settings.bg_max_errors_discard << std::endl;
    	  std::cout << "Min. score:                " << settings.bg_min_score << "..." << settings.bg_max_score << std::endl;
    	  std::cout << "Dynamic cycle interval:    " << settings.bg_dynamic_min_cycle << "..." << settings.bg_dynamic_max_cycle << std::endl;
    	  std::cout << std::endl;

    }
    if ( remarks.size()!=0 ) {
    	std::cout << "Warnings and remarks occurred:" << std::endl;
    	for ( auto it = remarks.begin(); it!=remarks.end(); ++it) {
    		std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    		std::cout << *it << std::endl;
    	}
		std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    	std::cout << std::endl;
    }

    return 0;
}

bool parseReadsArgument(AlignmentSettings & settings, std::vector< std::string > readsArg){

	CountType lenSum = 0;
	CountType length = 0;
	std::string length_string = "";
	char type;

	for ( auto read = readsArg.begin(); read != readsArg.end(); ++read ) {

		length_string = (*read).substr(0,(*read).length()-1);
		type = (*(*read).rbegin());

		try{
		std::stringstream( length_string ) >> length;
		} catch( std::bad_cast & ex ){
			std::cerr << "Error while casting length " << length_string << " to type uint16_t" << std::endl;
		}

		if ( type!='B' && type!='R' ) {
			std::cerr << "\'" << type << "\'" << " is no valid read type. Please use " << "\'R\'" << " for sequencing reads or "
					"\'B\'" << " for barcode reads." << std::endl;
			return false;
		}

		settings.seqs.push_back(SequenceElement(settings.seqs.size(), (type == 'R') ? ++settings.mates : 0, length));
		lenSum += length;

	}

	if ( lenSum!=settings.cycles ) {
		std::cerr << "Sum of defined reads does not equal the given number of cycles." << std::endl;
		return false;
	}

	return true;
}

bool parseBarcodeArgument(AlignmentSettings & settings, std::vector< std::string > barcodeArg ) {

	std::vector<uint16_t> barcode_lengths;

	for ( uint16_t seq_num = 0; seq_num < settings.seqs.size(); seq_num++ ) {

		// We are only interesed in Barcode sequences
		if ( !settings.getSeqById(seq_num).isBarcode() )
			continue;

		barcode_lengths.push_back( settings.getSeqById(seq_num).length );
	}

	for ( auto barcode = barcodeArg.begin(); barcode != barcodeArg.end(); ++barcode) {

		std::string valid_chars = seq_chars + "-";
		for(CountType i = 0; i != (*barcode).length(); i++){
			char c = (*barcode)[i];
			if ( valid_chars.find(c) == std::string::npos )
				return false;
		}

		std::vector<std::string> fragments;
		split(*barcode, '-', fragments);

		// check validity of barcode
		if ( barcode_lengths.size() != fragments.size())
			return false;



		for ( uint16_t num = 0; num != fragments.size(); num++ ) {
			if ( fragments[num].length() != barcode_lengths[num] ) {
				return false;
			}
		}

		// push back the fragments vector
		settings.barcodeVector.push_back(fragments);

	}

	return true;

}
