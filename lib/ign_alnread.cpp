#include "ign_alnread.h"

/**
 * Extend all seeds of a read in the PriLive alignment.
 * @param bc The new base call
 * @param settings Object containing the program settings
 * @param handleRead If false, the read is not yet in the PriLive handling. Just do the basic stuff in that case, no alignment handling!
 * @param createNewSeeds If true, new seeds are created for the respective seed.
 * @param testPrint Print some stuff for test reasons // TODO: remove before release!
 * @return
 * @author Tobias Loka
 */
void IgnoreReadAlignment::extend_alignment(char bc, KixRun* index, AlignmentSettings* settings, bool handleRead) {

	// move to the next cycle
	cycle += 1;
	assert( total_cycles >= cycle );

	// Null-call or quality under threshold --> set invalid base
	uint8_t qual = ((bc >> 2) & 63);
	if ( (bc == 0) || (qual < settings->min_qual) ){
		last_invalid = last_invalid > cycle ? last_invalid : cycle;
	}

	// Read is valid --> extend sequence
	unsigned mask = 3;
	if (flags != 0)
		appendNucleotideToSequenceStoreVector(revtwobit_repr(bc & mask));

	// Read not handled yet --> stop
	if ( cycle < K_PriLive || !handleRead ) {
		seeds.clear();
		return;
	}

	// Get the current k-mer from the stored sequence
	const std::string sequence = getSequenceString();
	std::string::const_iterator it_lastKmer = sequence.end() - K_PriLive;
	HashIntoType last_kmer = 0;
	hash_fw(it_lastKmer, sequence.end(), last_kmer, *settings, false);

	// Retrieve matching positions when the k-mer is valid
	GenomePosListType pos;
	if ( last_invalid + K_PriLive - 1 < cycle )
		pos = index->retrieve_positions(last_kmer);

	std::vector<bool> posWasUsedForExtension;
	posWasUsedForExtension.resize(pos.size(), false);

	// Number of additional matches for seeds that are created after a TRIMMED region (placeholder convertion)
	CountType leadingMatches = 0;

	// Handle trimmed k-mers
	if ( (pos.size() == 1) && (pos[0].gid == TRIMMED) ) {

		// Create placeholder if not existing yet
		if ( seeds.size() == 0 || seeds.front()->gid != TRIMMED ) {

			create_placeholder_seed(*settings);
			seeds.front()->num_matches = K_PriLive - 1;
			seeds.front()->cigar_data.back().length -= 1;
		}

		// Extend existing seeds
		for ( auto sd = seeds.begin(); sd != seeds.end(); ++sd ) {

			// Tailing NO_MATCH region of min_island_length --> Check for wobble match
			if ( (*sd)->cigar_data.back().offset == NO_MATCH && (*sd)->cigar_data.back().length == settings->bg_min_island_length) {

				// Compute expected position of the k-mer for the current seed
				PositionType seed_pos = (*sd)->getLastKmerPos() + 1;

				// Init data structure for wobble functions
				wobble_struct w = { sd, &sequence, last_kmer, NO_MATCH, seed_pos, 0 };

				// Consider previous wobbles for the current k-mer
				build_wobbled_kmer(w);

				// Get position matches (may be multiple due to trimmed matches)
				std::vector<uint8_t> wobble_offsets = wobble(w, index);

				// Save the wobbled seed(s)
				saveWobble(w, wobble_offsets);

				continue;

			}

			extendSeed(*sd, TRIMMED_MATCH, *settings);

			max_num_matches = std::max(max_num_matches, (*sd)->num_matches);
		}

		// Clear the pos list so nothing bad happens in the next steps
		pos.clear();

	}

	// UNTRIMMED k-mers
	else {

		// Set additional number of matches for seeds that are created after a TRIMMED region
		if ( seeds.size() != 0 && seeds.front()->gid == TRIMMED ) {
			leadingMatches = seeds.front()->num_matches - K_PriLive;
			seeds.remove(seeds.front());	// remove placeholder
		}

		auto cPos1 = pos.begin(), cPos2 = pos.begin(); // sliding window [cPos1, cPos2)

		// Iterate over all seeds
		for ( auto cSeed = seeds.begin(); cSeed != seeds.end(); ++cSeed ) {

			CigarVector* c = & ( (*cSeed)->cigar_data );

			// Extend NO_MATCH region if smaller than wobble check position
			if( c->back().offset == NO_MATCH && c->back().length < settings->bg_min_island_length ) {
				c->back().length += 1;
				max_num_matches = std::max(max_num_matches, (*cSeed)->num_matches);
				continue;
			}

			// Compute expected position of the k-mer for the current seed
			PositionType seed_pos = (*cSeed)->getLastKmerPos() + 1;

			// Init data structure for wobble functions
			wobble_struct w = { cSeed, &sequence, last_kmer, NO_MATCH, seed_pos, 0 };

			// Tailing NO_MATCH region of min_island_length --> Check for wobble match
			if ( (*c).back().offset == NO_MATCH && (*c).back().length == settings->bg_min_island_length) {

				// Consider previous wobbles for the current k-mer
				build_wobbled_kmer(w);

				// Get position matches (may be multiple due to trimmed matches)
				std::vector<uint8_t> wobble_offsets = wobble(w, index);

				// Save the wobbled seed(s)
				saveWobble(w, wobble_offsets);

				continue;
			}

			// Adjust the window in the position list (we need the window of size 0 to catch all gids)
			while( (cPos1!=pos.end()) && (cPos1->pos < seed_pos) )
				++cPos1;
			while( (cPos2!=pos.end()) && (cPos2->pos <= seed_pos) )
				++cPos2;

			// Search all positions in the window for the best matching extension of the seed
			DiffType best_offset = 1;  // set larger than search window
			GenomePosListIt best_match = cPos2; // set behind the last element of the window
			for(GenomePosListIt kmerHitIt = cPos1; kmerHitIt!=cPos2; ++kmerHitIt){
				if (kmerHitIt->gid == (*cSeed)->gid){

					// if offset gets bigger => Deletion in read
					int offset = kmerHitIt->pos - seed_pos;
					if ((best_match==cPos2)||(abs(offset) < abs(best_offset))) {
						best_match = kmerHitIt;
						best_offset = offset;
					}
				}
			}

			// Extend seed with match if a best match was found for this seed
			if (best_match != cPos2) {
				if(extendSeed(*cSeed, 0, *settings)) { // dont care about the offset

					/* mark matching pos such that it does not get converted into a new seed */
					posWasUsedForExtension[best_match-pos.begin()] = true;
					(*cSeed)->start_pos += best_offset; // TODO: check whether that makes sense
				}
			}

			// No best match found
			else{
				if ( last_invalid != cycle && build_wobbled_kmer(w) && index->kmerMatchesPosition(w.wobbled_kmer, seed_pos, (*cSeed)->gid) )
					extendSeed(*cSeed, 0, *settings);
				else
					extendSeed(*cSeed, NO_MATCH, *settings);
			}

			max_num_matches = std::max(max_num_matches, (*cSeed)->num_matches);
		} // END: loop through seeds
	} // END: if not trimmed

	// Filter all seeds that do not fulfill the criteria
	auto crit = [&] (USeed & s) {
		return lambda_ignore(s, cycle, settings);
	};

	seeds.erase(std::remove_if(seeds.begin(), seeds.end(), crit) , seeds.end());

	// Create new seeds if seeding criteria fulfilled
	if ( checkCreateSeeds(settings, leadingMatches) ) {
		if ( add_new_seeds(pos, posWasUsedForExtension, *settings, index, leadingMatches) != 0 )
			max_num_matches = std::max(max_num_matches, std::max(settings->bg_min_anchor_length, CountType(K_PriLive + leadingMatches)));
	}

}

bool IgnoreReadAlignment::checkCreateSeeds(AlignmentSettings * settings, CountType leadingMatches){

	// invalid cycles
	if ( cycle < settings->bg_min_anchor_length || last_invalid > cycle - settings->bg_min_anchor_length )
		return false;

	// already discarded
	if ( flags == 0 )
		return false;

	// Check if there are enough cycles to reach final score
	if ( cycle > total_cycles - ( settings->bg_max_score - ( std::max(settings->bg_min_anchor_length, CountType(K_PriLive + leadingMatches) ) ) ) )
		return false;

	return true;

}

void IgnoreReadAlignment::extendAnchors(SeedVec & newSeeds, AlignmentSettings & settings, KixRun * index) {

	CountType currentLength = K_PriLive;

	while ( currentLength < settings.bg_min_anchor_length  && newSeeds.size() > 0) {

		// get previous kmer of read
		currentLength += 1;

		const std::string sequence = getSequenceString();
		std::string::const_iterator it_lastKmer = sequence.end() - currentLength;
		HashIntoType last_kmer = 0;
		hash_fw(it_lastKmer, sequence.end(), last_kmer, settings, false);

		// check if previous kmer fits to new seed
		SeedVecIt seed = newSeeds.begin();
		auto pos = index->retrieve_positions(last_kmer);
		auto posit = pos.begin();

		while ( seed != newSeeds.end() ) {

			if ( posit == pos.end() ) {
				seed = newSeeds.erase(seed);
				continue;
			}

			// Handle that previous k-mers were trimmed (count as valid)
			if ( pos.size() == 1 && posit->gid == TRIMMED ) {
				(*seed)->num_matches += 1;
				(*seed)->start_pos -= 1;
				(*seed)->cigar_data.back().length += 1;
				++seed;
				continue;
			}

			// Position in PositionList lower than seed Position
			if ( posit->pos < (*seed)->start_pos - 1 || ( posit->pos == (*seed)->start_pos - 1 && posit->gid < (*seed)->gid )) {
				++posit;
				continue;
			}
			// Position in PositionList greater than seed Position
			else if ( posit->pos > (*seed)->start_pos - 1 || ( posit->pos == (*seed)->start_pos - 1 && posit->gid > (*seed)->gid )) {
				seed = newSeeds.erase(seed);
				continue;
			}
			// Position in PositionList equals seed Position
			else if ( posit->pos == (*seed)->start_pos - 1 && posit->gid == (*seed)->gid ) {
				(*seed)->num_matches += 1;
				(*seed)->start_pos -= 1;
				(*seed)->cigar_data.back().length += 1;
				++seed;
				++posit;
			}
		}
	}
}

std::string IgnoreReadAlignment::get_kmer_str(HashIntoType kmer){
	std::ostringstream stream;
	for ( CountType i=0; i<K_PriLive; i++){
		HashIntoType nextChar = ( ( kmer >> (2*(K_PriLive - i - 1)) ) & 3);
		stream << revtwobit_repr(nextChar);
	}
	return stream.str();
}

// Create new seeds from a list of kmer positions and add to current seeds
CountType IgnoreReadAlignment::add_new_seeds(GenomePosListType& pos, std::vector<bool> & posWasUsedForExtension, AlignmentSettings & settings, KixRun * index, CountType leadingMatches) {

	SeedVec newSeeds;

	for(GenomePosListIt it = pos.begin(); it != pos.end(); ++it) {
		if (posWasUsedForExtension[it - pos.begin()]) // if current reference hit was used at least once for seed extension
			continue;
		USeed s (new Seed);

		s->gid = it->gid;
		s->start_pos = it->pos - leadingMatches;
		s->num_matches = K_PriLive + leadingMatches;
		s->cigar_data.clear();

		// Don't support gap mask for ignore alignment, just add matches
		s->cigar_data.emplace_back(s->num_matches,0);

		newSeeds.push_back(std::move(s));

	}

	// only extend anchors when not converted from placeholder
	if ( leadingMatches == 0 )
		extendAnchors(newSeeds, settings, index);

	CountType numberNewSeeds = newSeeds.size();

	SeedVecIt newsit = newSeeds.begin();
	SeedVecIt sit = seeds.begin();

	// insert seed into sorted list of seeds
	while ( newsit != newSeeds.end() ) {
		if ( sit != seeds.end() && seed_compare_pos(*sit, *newsit)) { // if seed exists and elem has larger starting position than (*sit)
			++sit;
		}
		else {
			seeds.insert(sit, std::move(*newsit));
			++newsit;
		}
	}

	return numberNewSeeds;
}

bool IgnoreReadAlignment::lambda_ignore(USeed & s, CountType curr_cycle, AlignmentSettings* settings){

	/* Don't filter seeds that were extended *regularly* in this cycle */
	if( s->cigar_data.back().offset != NO_MATCH && s->cigar_data.back().length > settings->bg_min_island_length )
		return false;

	else {

		/* 1. Anchor not long enough */
		if (settings->bg_min_anchor_length > s->cigar_data.front().length )
			return true;

		/* 2. Wobbling did not succeed */
		if ( s->cigar_data.back().offset == NO_MATCH && s->cigar_data.back().length > settings->bg_min_island_length )
			return true;

		/* 4. Not enough cycles left to reach the necessary score */
		CountType overhang = ( s->cigar_data.back().offset == NO_MATCH ) * (settings->bg_min_island_length  - (settings->bg_min_island_length - s->cigar_data.back().length));
		CountType maxValue = s->num_matches + total_cycles - curr_cycle + overhang;
		if ( settings->bg_max_score > maxValue )
			return true;

	}

	/* Otherwise keep the seed. */
	return false;
}

bool IgnoreReadAlignment::build_wobbled_kmer(wobble_struct & w){

	// Remember if a wobble element occured while kmer building
	bool considered_wobble = false;

	// Fast access to the cigar vector
	CigarVector * cigVec = &((*(w.seed))->cigar_data);

	// Iterator through CIGAR elements (reverse)
	auto el = cigVec->rbegin();

	// Stop if the tailing CIGAR element is no wobble and is longer than a k-mer (so, k-mer will not change)
	if ( !isWobble(el->offset) && el->length >= K_PriLive)
		return false;

	// Iterator through sequence (reverse)
	auto nextBase = w.sequence->rbegin();

	// Add the current basecall to the k-mer
	w.wobbled_kmer = ( HashIntoType(twobit_repr(*nextBase)));
	++nextBase;

	// Store the current position in the stored sequence
	CountType base_pos = 1;

	// Iterate until the k-mer is filled
	while ( base_pos < K_PriLive ) {

		// End of CIGAR vector --> stop (should not happen)
		if ( el == cigVec->rend() )
			break;

		// Non-wobble --> add the next base of the sequence to the kmer
		if ( !isWobble(el->offset) ) {
			for ( int i = 0; i < el->length; i++,nextBase++,base_pos++ ) {
				if ( base_pos >= K_PriLive )
					goto whilebreak;
				w.wobbled_kmer |= ( HashIntoType(twobit_repr(*nextBase)) << 2*base_pos );
			}

		// Insertion in alignment --> skip the base (since it does not occur in the reference genome)
		} else if ( isInsertionWobble(el->offset) ) {
			nextBase++;
			considered_wobble = true;

		// Deletion in alignment --> Insert the base that occurs in the reference genome at the position
		} else if ( isDeletionWobble(el->offset) ) {
			w.wobbled_kmer |= ( HashIntoType(getWobbleBase(el->offset)%4) << 2*base_pos );
			base_pos++;
			considered_wobble = true;

		// SNP in alignment --> Replace the base that occurs in the reference genome by the SNP base
		} else {
			w.wobbled_kmer |= ( HashIntoType(getWobbleBase(el->offset)%4) << 2*base_pos );
			base_pos++;
			nextBase++;
			considered_wobble = true;
		}
		++el;
	}

	whilebreak:;

	// Store the last base in front of the k-mer to only consider this for wobbling
	if ( !isWobble(el->offset) )
		w.lastBaseBeforeKmer = twobit_repr(*nextBase);
	else if ( !isInsertionWobble(el->offset) )
		w.lastBaseBeforeKmer = getWobbleBase(el->offset)%4;
	else
		w.lastBaseBeforeKmer = twobit_repr(*(++nextBase));

	return considered_wobble;
}

std::vector<uint8_t> IgnoreReadAlignment::wobble(wobble_struct & w, KixRun* index) {

	CigarVector * cigVec = &((*(w.seed))->cigar_data);

	/* position shift due to new InDel */
	short wobble_shift = 0;

	std::vector<uint8_t> matches;

	uint8_t final_base;

	/* iterate through all wobble types */
	for ( uint8_t base = 0; base <= 8; base++ ) {

		final_base = base;

		// Parameter modifications when handling Indels (shift in position, base modification)
		if ( isInsertionWobble(getWobbleOffset(base)) ) {
			wobble_shift = -1;
			final_base += w.lastBaseBeforeKmer;
		}
		else if ( isDeletionWobble(getWobbleOffset(base)) ) {
			wobble_shift = 1;
		}

		/* wobble the k-mer */
		HashIntoType wobbled_kmer = wobbleKmer(w.wobbled_kmer, cigVec->back().length, final_base);

		DiffType matched = index->kmerMatchesPosition(wobbled_kmer, w.pos + wobble_shift, (*(w.seed))->gid);

		// Single match position
		if(  matched == 1) {
			// Remove trimmed matches and return only the position match
			matches.clear();
			matches.push_back(base);
			return matches;
		}

		// Trimmed match
		else if ( matched == 2 ) {
			// Stode the trimmed match in the matches vector
			matches.push_back(base);
		}
	}

	return matches;

}

void IgnoreReadAlignment::saveWobble(wobble_struct & w, std::vector<uint8_t> & wobble_offsets) {

	SeedVec::iterator cSeed = w.seed; // fast access to the seed
	CigarVector * c = &((*cSeed)->cigar_data);

	if ( wobble_offsets.size() == 1 ) {

		// Exactly one match --> Convert existing seed
		CountType matchLength = c->back().length + isDeletionWobble(getWobbleOffset(*wobble_offsets.begin()));
		c->back().length = 1;
		c->back().offset = getWobbleOffset(*wobble_offsets.begin());
		c->emplace_back(matchLength, 0);
		(*cSeed)->num_matches += (*c).back().length;
		max_num_matches = std::max(max_num_matches, (*cSeed)->num_matches);
	}

	else {

		// Matching wobble k-mers --> create new seeds and add them to the seed vector
		auto wobbles = wobble_offsets.begin();
		for ( wobbles = wobble_offsets.begin(); wobbles != wobble_offsets.end(); wobbles++) {
			USeed newSeed(new Seed());
			newSeed->cigar_data = (*cSeed)->cigar_data;
			newSeed->gid = (*cSeed)->gid;
			newSeed->num_matches = (*cSeed)->num_matches;
			newSeed->start_pos = (*cSeed)->start_pos;

			CountType matchLength = c->back().length + isDeletionWobble(getWobbleOffset(*wobbles));
			newSeed->cigar_data.back().length = 1;
			newSeed->cigar_data.back().offset = getWobbleOffset(*wobbles);
			newSeed->cigar_data.emplace_back(matchLength, 0);
			newSeed->num_matches += newSeed->cigar_data.back().length;
			max_num_matches = std::max(max_num_matches, newSeed->num_matches);
			cSeed = seeds.insert(cSeed, std::move(newSeed));
			cSeed++; // increment to be back at the position of the original seed
		}

		// Invalidate old seed by extending mismatch region
		(*c).back().length += 1;
	}
}
