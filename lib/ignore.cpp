#include <iostream>
#include <ignore.h>

//-------------------------------------------------------------------//
//----------------  The Ignore Handler Class  ------------------//
//-------------------------------------------------------------------//

// ---------- File handling ----------

std::string IgnoreHandler::get_ignore_file(std::string base) {
	return get_ignore_file(mate, base);
}

std::string IgnoreHandler::get_ignore_file(uint16_t mt, std::string base) {

	std::ostringstream path_stream;

	if(base=="")
		base = root;

	path_stream << base << lanePath(lane) << "/s_" << lane << "_" << tile << "." << mt << ".ignore";

	return path_stream.str();
}


std::string IgnoreHandler::get_cont_file(std::string base) {

	std::ostringstream path_stream;

	if(base=="")
		base = root;

	// path_stream << base << "/ignore_reads" << "." << mate << ".cont";
	path_stream << base << "/filtered" << ".cont";

	return path_stream.str();
}

std::string IgnoreHandler::get_names_file() {

	std::ostringstream path_stream;

	path_stream << root << lanePath(lane) << "/s_" << lane << "_" << tile << ".names";

	return path_stream.str();
}

void IgnoreHandler::init_ignore_file() {

	std::string out_fname = get_ignore_file(settings->temp_dir);

	// Initialize with 0 discarded reads
	FILE * output(fopen(out_fname.c_str(), "wb"));
	if (!output) {
		std::cerr << "Could not open file " << out_fname << " for writing." << std::endl;
		return;
	}
	uint16_t written = fwrite(&num_ignored_reads, sizeof(uint32_t), 1, output);
	fclose(output);

	if ( written != 1 ) {
		std::cerr << "Initialization of ignore file failed. Try again later." << std::endl;
		std::this_thread::sleep_for(std::chrono::minutes(5));
		init_ignore_file();
	}

}

bool IgnoreHandler::init_cont_file(){

	std::string out_fname = get_cont_file(settings->out_dir);

	// init (or clear) the contamination output file
	FILE * output(fopen(out_fname.c_str(), "w"));
	if (!output) {
		return false;
	}
	fclose(output);

	return true;
}

void IgnoreHandler::load_ignored_reads() {

	num_ignored_reads = 0;
	considered_reads_map.clear();
	uint32_t next_read;

	// Load file from the same mate in all but the first cycles.
	if ( cycle > 1 || mate == 1 ) {

		std::string in_fname = get_ignore_file(settings->temp_dir);

		std::vector<char> data = read_binary_file(in_fname);
		char* d = data.data();

		memcpy(&num_ignored_reads, d, sizeof(uint32_t));

		for(uint32_t i = 1; i<=num_ignored_reads; i++){
			memcpy(&next_read, d + (i * sizeof(uint32_t)), sizeof(uint32_t));
			considered_reads_map.insert(deserialize_read(next_read));
		}
	}

	// Load file from the previous mate in the first cycle
	else {

		std::string in_fname = get_ignore_file(mate-1, settings->temp_dir);

		std::vector<char> data = read_binary_file(in_fname);
		char* d = data.data();

		uint32_t num_reads = 0;
		memcpy(&num_reads, d, sizeof(uint32_t));

		for(uint32_t i = 1; i<=num_reads; i++){
			memcpy(&next_read, d + (i * sizeof(uint32_t)), sizeof(uint32_t));
			auto deserialized_read = deserialize_read(next_read);

			// Only store discarded and WRONG_BARCODE reads
			if ( deserialized_read.second == IgnoreStatus::WRONG_BARCODE
					|| deserialized_read.second == IgnoreStatus::DISCARDED
					|| deserialized_read.second == IgnoreStatus::NEW_DISCARDED ) {
				considered_reads_map.insert(deserialize_read(next_read));
				num_ignored_reads += 1;
			}
		}
	}
}

void IgnoreHandler::add_cont_reads(std::vector<uint32_t>& reads){

	// No newly discarded reads: stop
	if ( reads.size() == 0 )
		return;

	// sort reads for iteration
	std::sort(reads.begin(), reads.end());
	std::string out_fname = get_cont_file(settings->out_dir);
	std::string names_fname = get_names_file();

	uint32_t curr_names_line = 0;
	std::string name;

	/* Open names file */
	std::ifstream names_file(names_fname.c_str());

	/* write read names to cont file */
	settings->cont_file_mutex.lock();
	FILE * output(fopen(out_fname.c_str(), "a"));

	for ( auto rd = reads.begin(); rd != reads.end(); ++rd ) {

		// if names file exist, use names of the reads
		if (names_file) {

			// Ignore all lines before the next read
			while (curr_names_line <= (*rd) ) {
				getline(names_file, name);
				curr_names_line++;
			}

		// if names file does not exist, build a unique identifier (for the respective tile/lane)
		} else {
			name = "read." + std::to_string(*rd);
		}
		std::ostringstream line;
		line << getSeqCycle(cycle, settings, settings->getSeqByMate(mate).id) << '\t' << lane << '\t' << tile << '\t' << name << '\n';
		fputs (line.str().c_str(), output);
	}

	names_file.close();
	fclose (output);
	settings->cont_file_mutex.unlock();
}

void IgnoreHandler::update_ignore_file(bool is_discard_cycle, bool bcl_manip) {

	std::string ignore_fname = get_ignore_file(settings->temp_dir);

	uint32_t elements = considered_reads_map.size();

	// separate new discarded reads since they must be deleted from the bcl files
	std::vector<uint32_t> all_discarded_reads;
	std::vector<uint32_t> new_discarded_reads;

	// Read binary data of considered reads
	std::vector<char> data ( (elements + 1) * sizeof(uint32_t) );
	char* d = data.data();

	// Save considered reads number in data buffer
	memcpy(d,&elements,sizeof(uint32_t));
	d += sizeof(uint32_t);

	// Iterate through all considered reads
	for ( auto elem = considered_reads_map.begin(); elem != considered_reads_map.end(); ++elem ) {

		// If discard cycle, store discarded reads in two different vectors
		if ( elem->second == IgnoreStatus::DISCARDED && is_discard_cycle )
			all_discarded_reads.push_back(elem->first);
		else if ( elem->second == IgnoreStatus::NEW_DISCARDED && is_discard_cycle ) {
			all_discarded_reads.push_back(elem->first);
			new_discarded_reads.push_back(elem->first);
			setIgnoreStatus(elem->first, IgnoreStatus::DISCARDED);
		}

		// serialize read number & status
		uint32_t serialized_value = serialize_read(elem->first, elem->second);

		// write serialized read to data buffer
		memcpy(d,&serialized_value,sizeof(uint32_t));
		d += sizeof(uint32_t);
	}

	// write data buffer to file
	uint64_t written = write_binary_file(ignore_fname, data);
	while ( written != data.size() ) {
		std::this_thread::sleep_for(std::chrono::minutes(1));
		written = write_binary_file(ignore_fname, data);
	}

	// BCL file manipulation
	if( is_discard_cycle && bcl_manip){

		// Compute current bcl manipulation delay
		uint16_t current_delay = settings->cycleDelay == 0 ? 0 : ( settings->getSeqByMate(mate).length - cycle ) / ( settings->getSeqByMate(mate).length / settings->cycleDelay );
		uint16_t last_delay = settings->cycleDelay == 0 ? 0 : ( settings->getSeqByMate(mate).length - (cycle - settings->bg_discard_frequency) ) / ( settings->getSeqByMate(mate).length / settings->cycleDelay );

		uint16_t lastDiscardedCycle = cycle - settings->bg_discard_frequency - last_delay;
		uint16_t maxDiscardCycle = cycle >= settings->getSeqByMate(mate).length ? cycle : cycle - current_delay;

		if ( ! settings->decreasing_delay ) {
			lastDiscardedCycle = cycle - settings->bg_discard_frequency - settings->cycleDelay;
			maxDiscardCycle = cycle >= settings->getSeqByMate(mate).length ? cycle : cycle - settings->cycleDelay;
		}


		// Remove all discarded reads from the latest bcl files
		removeFromBcl(all_discarded_reads, getSeqCycle(lastDiscardedCycle, settings, settings->getSeqByMate(mate).id) + 1, getSeqCycle(maxDiscardCycle, settings, settings->getSeqByMate(mate).id));

		// Remove "new" discarded reads from the older bcl files
//		uint16_t firstSeqCycleToRemove = getSeqCycle(1, settings, settings->getSeqByMate(mate).id);
//		removeFromBcl(new_discarded_reads, firstSeqCycleToRemove, getSeqCycle(lastDiscardedCycle, settings, settings->getSeqByMate(mate).id));
		removeFromBcl(new_discarded_reads, 1, getSeqCycle(lastDiscardedCycle, settings, settings->getSeqByMate(mate).id));
		add_cont_reads(new_discarded_reads);

	// bcl manipulation off -> fill cont file
	} else if ( is_discard_cycle ) {
		add_cont_reads(new_discarded_reads);
	}
}

uint32_t IgnoreHandler::serialize_read(uint32_t deserialized_read, IgnoreStatus status){

	uint32_t serialized_read = INVALID;

	/* Return INVALID if read number is too high */
	if(deserialized_read > ~( uint32_t(7) << 29)){
		std::cerr << "Serialization of read in ignore alignment failed (max. "<< ~((uint32_t)7 << 29) << "reads per tile are supported!)" << std::endl;
		return serialized_read;
	}

	// Paused is saved as Handled.
	if ( status == IgnoreStatus::PAUSED )
		status = IgnoreStatus::HANDLED;

	// status is invalid if using more than 3 bits.
	if ( uint8_t(status) > uint8_t(7) )
		return INVALID;

	// Save the status in the first three bits of the uint32_t
	serialized_read = ( deserialized_read & ~(uint32_t(7) << 29) ) | ( uint32_t(status) << 29 );

	return serialized_read;
}

std::pair<uint32_t, IgnoreStatus> IgnoreHandler::deserialize_read(uint32_t serialized_read){

	/* read status flag and set the status respecitvely (enum values match the flags) */
	IgnoreStatus stat = IgnoreStatus(serialized_read >> 29);

	/* return pair with deserialized read number and status */
	return std::pair<uint32_t, IgnoreStatus>(serialized_read & ~((uint32_t)7 << 29), stat);
}


// ---------- Pre Ignore status handling ----------

IgnoreStatus IgnoreHandler::updateForegroundAlignmentStatus(uint32_t read, CountType min_errors) {

	switch(getIgnoreStatus(read)){
	case IgnoreStatus::HANDLED:
	case IgnoreStatus::PAUSED:
//		if ( checkHandleCriteria(min_errors) )
//			handleRead(read);
//		if ( checkAllowDiscardingCriteria(min_errors) )
//			allowDiscardingRead(read);
//		else if ( checkPauseCriteria(min_errors) )
//			pauseRead(read);
		if ( checkAllowDiscardingCriteria( min_errors ) )
			allowDiscardingRead( read );
		else if ( checkPauseCriteria( min_errors ) )
			pauseRead( read );
		else
			handleRead( read );
		break;
	case IgnoreStatus::UNHANDLED:
//		if ( checkHandleCriteria(min_errors) )
//			handleRead(read);
//		if ( checkAllowDiscardingCriteria(min_errors) )
//			allowDiscardingRead(read);
		if ( checkAllowDiscardingCriteria( min_errors ) )
			allowDiscardingRead( read );
		else if ( checkHandleCriteria( min_errors ) )
			handleRead( read );
		// Don't allow UNHANDLED->PAUSED.
		break;
	case IgnoreStatus::PRE_DISCARDED:
		if ( checkAllowDiscardingCriteria(min_errors) )
			discardRead(read);
		break;
	case IgnoreStatus::ALLOW_DISCARDING:
		if ( !checkAllowDiscardingCriteria(min_errors) )
			handleRead(read);
		if ( checkPauseCriteria(min_errors) )
			pauseRead(read);
		break;
	default:
		break;
	}

	// change to "dont handle" if the necessary number of errors to discard a read cannot be reached any more
	if ( min_errors < settings->bg_max_errors_discard ) {
		if ( (settings->getSeqByMate(mate).length - cycle ) / settings->kmer_span + 2 + min_errors < settings->bg_max_errors_discard )
			dontHandle(read);
	}

	return getIgnoreStatus(read);

}

IgnoreStatus IgnoreHandler::updateBackgroundAlignmentStatus(uint32_t read, CountType max_num_matches) {

	switch(getIgnoreStatus(read)){
	case IgnoreStatus::HANDLED:
	case IgnoreStatus::PAUSED:
		if ( max_num_matches >= settings->bg_max_score )
			preDiscardRead(read);
		break;
	case IgnoreStatus::ALLOW_DISCARDING:
		if ( max_num_matches >= getDynamicIgnoreScore() )
			discardRead(read);
		break;
	default:
		break;
	}

	// change to dont handle if the necessary score to discard a read cannot be reached any more
	if ( (settings->getSeqByMate(mate).length - cycle ) + max_num_matches + settings->bg_min_island_length < settings->bg_max_score &&
			(settings->getSeqByMate(mate).length - cycle ) < settings->bg_max_score - settings->bg_min_anchor_length + 1 )
		dontHandle(read);

	return getIgnoreStatus(read);
}



// ---------- BCL file manipulation ----------

void IgnoreHandler::removeFromBcl(std::vector<uint32_t>& reads, uint16_t first_cycle, uint16_t last_cycle){

	if(reads.size()==0)
		return;

	BCL_Modification_Job bcl_job("", reads, first_cycle, last_cycle, lane, tile);
	settings->io_queue.push(bcl_job);

}
