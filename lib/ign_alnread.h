#ifndef IGN_ALNREAD_H
#define IGN_ALNREAD_H

#include "headers.h"
#include "definitions.h"
#include "kindex.h"
#include "tools.h"
#include "alnread.h"
#include <seqan/basic.h>
#include <seqan/bam_io.h>



//-------------------------------------------------------------------//
//------  The Ignore Read-Alignment class  --------------------------//
//-------------------------------------------------------------------//
class IgnoreReadAlignment : public ReadAlignment {
public:

	/**
	 *  Struct containing all relevant elements for wobbling processes.
	 *  @author Tobias Loka
	 */
	struct wobble_struct {

		/** Iterator pointing to the current seed. */
		SeedVec::iterator seed;

		/** Pointer to the sequence of the read. */
		const std::string * sequence;

		/** The wobbled k-mer */
		HashIntoType wobbled_kmer;

		/** New offset after wobbling */
		DiffType new_offset;

		/** The position */
		PositionType pos;

		/** The last base before the wobbled kmer (important for new insertions) */
		CountType lastBaseBeforeKmer;

	};

	/**
	 * Extend alignment for a read. Equivalent to exntend_alignment function of the foreground alignment.
	 * @param bc The next basecall as received from the sequencer.
	 * @param index Pointer to the kmer index
	 * @param settings Pointer to the alignment settings
	 * @param readAlreadyIgnored true if the read is alyready handled in background alignment. false otherwise.
	 * @author Tobias Loka
	 */
	void extend_alignment(char bc, KixRun* index, AlignmentSettings* settings, bool readAlreadyIgnored);

	/**
	 * Add new seeds for a read in ignore alignment. This is also used for placeholder convertion.
	 * @param pos List of positions for the current kmer in the reference genome.
	 * @param posUsedForExtension List of flags to indicate which positions were used for seed extension (no seed creation for these).
	 * @param settings The alignment settings.
	 * @param index The (ignore alignment) reference genome.
	 * @param leadingMatches Number of leading matches ( !=0 for placeholder convertion ).
	 * @return Number of created seeds.
	 * @author Tobias Loka
	 */
	CountType add_new_seeds(GenomePosListType& pos, std::vector<bool> & posWasUsedForExtension, AlignmentSettings & settings, KixRun * index, CountType leadingMatches);

	/**
	 * Extend anchors during seed creation ( to only create seeds with a minimum anchor size ).
	 * @param seeds Container with new seed candidates (to be extended).
	 * @param settings The alignment settings.
	 * @param index The (ignore alignment) reference genome.
	 * @author Tobias Loka
	 */
	void extendAnchors(SeedVec & seeds, AlignmentSettings & settings, KixRun * index);

	/**
	 * Perform known wobbles from previous cycles for the current seed.
	 * @param w Struct containing all necessary data for wobbling
	 * @return true, if the kmer was affected by wobbles
	 * @author Tobias Loka
	 */
	bool build_wobbled_kmer(wobble_struct & w);

	/**
	 * Check if the current k-mer matches with a SNP or 1bp-InDel at a defined position (wobbling)
	 * @param w Struct with all information for wobbling.
	 * @param index Pointer to the kmer index
	 * @return Container with matching wobbles. Can (only) be >1 when all matching wobbles were trimmed in the reference genome.
	 */
	std::vector<uint8_t> wobble(wobble_struct & w, KixRun* index);

	/**
	 * Save the wobbled matches in the seed vector.
	 * @param w Struct containing all necessary data for wobbling.
	 * @param wobble_offsets List of matching wobble offsets.
	 * @author Tobias Loka
	 */
	void saveWobble(wobble_struct & w, std::vector<uint8_t> & wobble_offsets);

	/**
	 * Check whether new seeds shall be created for the respective read
	 * @param settings Pointer to the alignment settings
	 * @param leadingMatches >0 when converting a placeholder. Has influence on the maximal score that can be reached with created seeds.
	 * @return true, if criteria for the creation of new seeds in the ignore alignment are fulfilled. false otherwise.
	 * @author Tobias Loka
	 */
	bool checkCreateSeeds(AlignmentSettings * settings, CountType leadingMatches=0);

	/**
	 * Seed filter function for PriLive. // TODO: Check if it can be IgnoreReadAlignment::lamda_ignore
	 * @param s The seed to decide about filtering
	 * @param curr_cycle The current cycle of the mate (read)
	 * @param settings Object containing the program settings
	 * @return true: filter seed; false: keep seed
	 * @author Tobias Loka
	 */
	bool lambda_ignore(USeed & s, CountType curr_cycle, AlignmentSettings* settings);

	/**
	 * Return a kmer in string format. Only for test reasons.
	 * @kmer The kmer key.
	 * @return The kmer as string.
	 * @author Tobias Loka
	 */
	std::string get_kmer_str(HashIntoType kmer);

	/**
	 * Default destructor.
	 */
	~IgnoreReadAlignment(){};
};


#endif /* ALNREAD_H */
