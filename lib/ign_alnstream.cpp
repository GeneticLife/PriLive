#include <iostream>
#include <boost/program_options.hpp>
#include <ign_alnstream.h>

//----------------  Ignore Controller Class   -------------------//

bool IgnoreAlignmentController::initIgnoreAlignment(uint16_t lane, uint16_t tile, uint16_t mate, uint16_t rlen) {

	IgnoreHandler ign_handl (lane, tile, (uint16_t) 0, mate, root, settings, false);

	/* Init ignore file */
	ign_handl.init_ignore_file();

	/* Init .ia files and data structure for ignore alignment */
	StreamedIgnoreAlignment is (lane, tile, root, rlen);
	is.init_alignment(mate, settings);

	return true;
}

uint64_t IgnoreAlignmentController::align(Task & t, KixRun* idx, KixRun* ign_idx) {

	/* Init ignore handler */
	auto ignore_handler = std::make_shared<IgnoreHandler>(t.lane, t.tile, t.cycle, t.seqEl.mate, root, settings, settings->hasIgnoreAlignment());

	StreamedAlignment s (t.lane, t.tile, root, t.seqEl.length);
	StreamedIgnoreAlignment is (t.lane, t.tile, root, t.seqEl.length);

	uint64_t num_seeds;
	uint64_t num_ign_seeds;

	/* Extend foreground alignment */
	num_seeds = s.extend_alignment(t.cycle, t.seqEl.id, t.seqEl.mate, idx, settings, ignore_handler);

	/* Extend ignore alignment (if activated)*/
	if ( settings->hasIgnoreAlignment() ) {

		/* extend read alignments */
		num_ign_seeds = is.extend_alignment(t.cycle, t.seqEl.id, t.seqEl.mate, ign_idx, settings, ignore_handler);

		/* Update ignore file considering the determined bcl manipulation criterion */
		ignore_handler->update_ignore_file( is_discard_cycle(t), check_bcl_manipulation() );
	}

	// Remove old temp files. Ignore keep_aln_files settings when host removal is activated (sequences could be recovered)
	if ( ! settings->keep_aln_files  || settings->hasIgnoreAlignment() ) {
		s.remove_old_alignment_file(t.cycle, t.seqEl.mate, settings);
	}

	if ( settings->hasIgnoreAlignment() ) {
		is.remove_old_alignment_file(t.cycle, t.seqEl.mate, settings);

	}

	// Console output
   	std::ostringstream out;
   	out << getCurrentTimeString() << ": Task [" << t << "]: " << num_seeds;
	if ( settings->hasIgnoreAlignment() )
		out << " | " << num_ign_seeds;
	out << " seeds.";
	consoleOut(out.str(), &settings->console_mutex);

	return num_seeds;
}

CountType IgnoreAlignmentController::extend_barcode(uint16_t lane, uint16_t tile, uint16_t bc_cycle, uint16_t read_no, AlignmentSettings * settings) {

	StreamedAlignment s (lane, tile, root, settings->getSeqById(read_no).length);
	StreamedIgnoreAlignment is (lane, tile, root, settings->getSeqById(read_no).length);

	CountType handled_mates = 0;

	// Iterate through all mates
	for (CountType mate = 1; mate <= settings->mates; mate++ ) {

		// Ignore excluded mates
		if ( std::find(settings->exclude_mates.begin(), settings->exclude_mates.end(), mate) == settings->exclude_mates.end() ) {

			SequenceElement seqEl = settings->getSeqByMate(mate);

			// If current mate is already processed, the cycle is it's total length (finished). Else, it's cycle is 0 (not started yet).
			CountType current_mate_cycle = seqEl.id > read_no ? 0 : seqEl.length;

			// Actual barcode extension call for both alignments
			s.extend_barcode(bc_cycle, current_mate_cycle, read_no, mate, settings);
			if ( settings->hasIgnoreAlignment() )
				is.extend_barcode(bc_cycle, current_mate_cycle, read_no, mate, settings);

			handled_mates++;
		}
	}

	return handled_mates;
}

bool IgnoreAlignmentController::check_bcl_manipulation() {

	// BCL manipulation deactivated
	if ( settings->bg_discard_frequency == 0 )
		return false;

	return true;
}

bool IgnoreAlignmentController::is_discard_cycle(Task & t) {

	// Not enough barcodes available
	if ( t.seqEl.id + 1 - t.seqEl.mate < settings->bg_barcodes_before_discard  && !settings->keep_all_barcodes)
		return false;

	// Invalid cycle (>length)
	if ( t.cycle > t.seqEl.length )
		return false;

	// Last cycle of the read
	if ( t.cycle == t.seqEl.length )
		return true;

	// First cycle to discard not yet reached
	if ( t.cycle < settings->bg_first_cycle_to_discard )
		return false;

	// check modulo (discard frequency)
	return ( settings->bg_discard_frequency != 0 && t.cycle % settings->bg_discard_frequency == 0 );
}

//-------------------------------------------------------------------//
//------  The StreamedIgnoreAlignment Class  ------------------------//
//-------------------------------------------------------------------//

std::string StreamedIgnoreAlignment::get_alignment_file(uint16_t cycle, uint16_t mate, std::string base) {
	if (base == "") {
		base = root;
	}
	std::ostringstream path_stream;
	path_stream << base << lanePath(lane) << "/s_"<< lane << "_" << tile << "." << mate << "." << cycle << ".ia";
	return path_stream.str();
}

uint64_t StreamedIgnoreAlignment::extend_alignment(uint16_t cycle, uint16_t read_no, uint16_t mate, KixRun* index, AlignmentSettings* settings,
		std::shared_ptr<IgnoreHandler> ignore_handler) {

	// 0. If no ignore handler given, assume that nothing shall be done.
	//-------------------------
	if(!ignore_handler)
		return 0;

	// 1. Open the input file
	//-----------------------
	std::string in_fname = get_alignment_file(cycle-1, mate, settings->temp_dir);
	std::string out_fname = get_alignment_file(cycle, mate, settings->temp_dir);
	std::string bcl_fname = get_bcl_file(cycle, settings, read_no);
	std::string filter_fname = get_filter_file();

	iAlnStream input ( settings->block_size, settings->compression_format );
	input.open(in_fname);
	assert(input.get_cycle() == cycle-1);
	assert(input.get_lane() == lane);
	assert(input.get_tile() == tile);
	assert(input.get_root() == root);
	assert(input.get_rlen() == rlen);

	uint32_t num_reads = input.get_num_reads();


	// 2. Open output stream
	//----------------------------------------------------------
	oAlnStream output (lane, tile, cycle, root, rlen, num_reads, settings->block_size, settings->compression_format);
	output.open(out_fname);

	// 3. Read the full BCL file (this is not too much)
	//-------------------------------------------------
	BclParser* basecalls = ignore_handler->getBasecalls();
	basecalls->reset();

	// extract the number of reads from the BCL file
	uint32_t num_base_calls = basecalls->size();
	assert(num_base_calls == num_reads);


	// 4. Load the filter flags if filter file is available
	// ----------------------------------------------------
	FilterParser filters;
	if (file_exists(filter_fname)) {
		filters.open(filter_fname);
		// extract the number of reads from the filter file
		uint32_t num_reads_filter = filters.size();

		if (num_reads != num_reads_filter){
			std::string msg = std::string("Number of reads in filter file (") + std::to_string(num_reads_filter) + ") does not match the number of reads in the BCL file (" + std::to_string(num_reads) + ").";
			throw std::length_error(msg.c_str());
		}
	}

	// 5. Extend alignments 1 by 1
	//-------------------------------------------------
	uint64_t num_seeds = 0;
	for (uint32_t i = 0; i < num_reads; ++i) {

		char bc = basecalls->next();

		/* Get and downcast IgnoreReadAlignment object */
		IgnoreReadAlignment* ra = dynamic_cast<IgnoreReadAlignment *>(input.get_alignment(new IgnoreReadAlignment()));

		/* If read already reached score in ignore alignment */
		if ( ra->flags == 0 ){
			ra->cycle += 1;
			output.write_alignment(ra);
			delete ra;
			continue;
		}

		/** Read not handled in ignore alignment -> do only basic stuff */
		if ( !ignore_handler->hasHandledReads() || !(ignore_handler->isInIgnoreAlignment(i)) ) {
			ra->extend_alignment(bc, index, settings, false);
		}

		/** Otherwise, extend alignment. */
		else {

			ra->extend_alignment(bc, index, settings, true);

			// update status and disable if criteria fulfilled or read will no longer be handled.
			ignore_handler->updateBackgroundAlignmentStatus(i, ra->max_num_matches);
			if ( ignore_handler->isNewDiscarded(i) || ignore_handler->getIgnoreStatus(i)==IgnoreStatus::DONT_HANDLE )
				ra->disable();

		}

		num_seeds += ra->seeds.size();
		output.write_alignment(ra);

		delete ra;
	}

	// 6. Close files
	//-------------------------------------------------
	if (!(input.close() && output.close())) {
		std::cerr << "Could not finish alignment!" << std::endl;
	}

	return num_seeds;
}

void StreamedIgnoreAlignment::extend_barcode(uint16_t bc_cycle, uint16_t read_cycle, uint16_t read_no, uint16_t mate, AlignmentSettings * settings) {

	IgnoreHandler ignoreHandler(lane, tile, read_cycle, mate, root, settings, settings->hasIgnoreAlignment());

	// 1. Open the input file
	//-----------------------

	std::string in_fname = get_alignment_file(read_cycle, mate, settings->temp_dir);
	  std::string bcl_fname = get_bcl_file(bc_cycle, settings, read_no);
	  std::string filter_fname = get_filter_file();

	  iAlnStream input ( settings->block_size, settings->compression_format );
	  input.open(in_fname);
	  assert(input.get_cycle() == read_cycle);
	  assert(input.get_lane() == lane);
	  assert(input.get_tile() == tile);
	  assert(input.get_root() == root);

	  uint32_t num_reads = input.get_num_reads();


	  // 2. Open output stream
	  //----------------------------------------------------------
	  std::string out_fname = in_fname + ".temp";
	  oAlnStream output (lane, tile, read_cycle, root, input.get_rlen(), num_reads, settings->block_size, settings->compression_format);
	  output.open(out_fname);



	  // 3. Read the full BCL file (this is not too much)
	  //-------------------------------------------------
	  BclParser basecalls;
	  basecalls.open(bcl_fname);

	  // extract the number of reads from the BCL file
	  uint32_t num_base_calls = basecalls.size();
	  assert(num_base_calls == num_reads);

	  // 4. Extend barcode sequence
	  //-------------------------------------------------
	  for (uint64_t i = 0; i < num_reads; ++i) {
		char bc = basecalls.next() & 3; // only the nucleotide, ignore the quality.
	    ReadAlignment* ra = input.get_alignment();
	    ra->appendNucleotideToSequenceStoreVector(revtwobit_repr(bc), true);

	    // Flag read as WRONG_BARCODE to indicate that the barcode does not match (read will no longer be aligned and never filtered)
	    // TODO: Is done for each mate. Check if it's worth to change it (runtime should not be too high?)
	    if ( !settings->keep_all_barcodes && bc_cycle == settings->seqs[read_no].length && ra->getBarcodeIndex(settings) == NO_MATCH ) {
	    	ra->disable();
	    	ignoreHandler.wrongBarcode(i);
	    }

	    output.write_alignment(ra);
	    delete ra;
	  }

	  // Filter bcl files when ignore alignment finished and the prescribed number of barcodes is available
	  // ( When the barcode does not match, IgnoreStatus will be NO_MATCH -> no discard of those reads )
	  bool is_discard_cycle = ( settings->getSeqById(read_no).length == bc_cycle ); // all bases of current barcode available
	  is_discard_cycle = ( read_cycle == settings->getSeqByMate(mate).length ) ? is_discard_cycle : false; // Mate already finished
	  is_discard_cycle = ( ( read_no + 1 - mate ) >= settings->bg_barcodes_before_discard) ? is_discard_cycle : false; // Enough barcodes available
	  ignoreHandler.update_ignore_file( is_discard_cycle, settings->bg_discard_frequency != 0);

	  // 5. Close files
	  //-------------------------------------------------
	  if (!(input.close() && output.close())) {
	    std::cerr << "Could not finish alignment!" << std::endl;
	  }

	  // 6. Move temp out file to the original file.
	  //-------------------------------------------
	  std::rename(out_fname.c_str(), in_fname.c_str());
}


// ---------- BCL file manipulation ----------

bool removeFromBcl(BCL_Modification_Job* job, AlignmentSettings* settings, bool removeBarcodes){

	if ( job->reads.size()==0 )
		return true;

	auto cycle = job->cycle_start;

	while(cycle<=job->cycle_end){

		if ( ! removeBarcodes && settings->getMateBySeqCycle(cycle) == 0 ) {
			cycle++;
			continue;
		}

		// Don't use the job fname here since we modify several files.
		std::string out_fname = bcl_name(settings->root, job->lane, job->tile, cycle);
		std::string safety_out_fname = out_fname + ".filtered";

		// File doesn't exists
		if(!file_exists(out_fname)){
			return false;
		}

		BclParser bcl_data;
		bcl_data.open(out_fname);

		for (auto read = job->reads.begin(); read != job->reads.end(); ++read) {
			bcl_data.manipulateBaseCall(*read, '\0');
		}

		uint64_t written = write_binary_file(safety_out_fname, bcl_data.getData());
		if ( written == bcl_data.getData().size()) {
			std::remove(out_fname.c_str());
			std::rename(safety_out_fname.c_str(), out_fname.c_str());
		} else {
			return false;
		}


		cycle += 1;
	}

	return true;
}

